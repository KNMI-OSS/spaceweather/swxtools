import pandas as pd
import io

def goes_primary_secondary_df():
    # Source docs for definitions of primary vs secondary:
    #   https://www.ngdc.noaa.gov/stp/satellite/goes/doc/GOES_XRS_readme.pdf
    #   https://www.swpc.noaa.gov/news/goes-14-15-are-now-storage-mode
    #   https://www.swpc.noaa.gov/news/goes-18-now-secondary-data-source-replacing-goes-17
    #
    t_now = pd.Timestamp.utcnow().strftime("%Y-%m-%dT%H:%M:%S")
    goes_primaries = f'''
    startDate             stopDate               primary  secondary
    1986-01-01T00:00:00   1988-01-26T00:00:00          6          5
    1988-01-26T00:00:00   1994-12-11T00:00:00          7          6
    1994-12-11T00:00:00   1995-03-01T00:00:00          7          8
    1995-03-01T00:00:00   1998-07-27T00:00:00          8          7
    1998-07-27T00:00:00   2003-04-08T15:00:00          8         10
    2003-04-08T15:00:00   2003-05-15T15:00:00         10         12
    2003-05-15T15:00:00   2006-06-28T14:00:00         12         10
    2006-06-28T14:00:00   2007-01-01T00:00:00         12         11
    2007-01-01T00:00:00   2007-04-12T00:00:00         10         11
    2007-04-12T00:00:00   2007-11-21T00:00:00         11         10
    2007-11-21T00:00:00   2007-12-05T00:00:00         11          0
    2007-12-05T00:00:00   2007-12-18T00:00:00         11         10
    2007-12-18T00:00:00   2008-02-10T16:30:00         11          0
    2008-02-10T16:30:00   2009-12-01T00:00:00         10          0
    2009-12-01T00:00:00   2010-09-01T00:00:00         14          0
    2010-09-01T00:00:00   2010-10-28T00:00:00         14         15
    2010-10-28T00:00:00   2011-09-01T00:00:00         15          0
    2011-09-01T00:00:00   2012-10-23T16:00:00         15         14
    2012-10-23T16:00:00   2012-11-19T16:31:00         14         15
    2012-11-19T16:31:00   2015-01-26T16:01:00         15          0
    2015-01-26T16:01:00   2015-05-21T18:00:00         15         13
    2015-05-21T18:00:00   2015-06-09T16:25:00         14         13
    2015-06-09T16:25:00   2016-05-03T13:00:00         15         13
    2016-05-03T13:00:00   2016-05-12T17:30:00         13         14
    2016-05-12T17:30:00   2016-05-16T17:00:00         14         13
    2016-05-16T17:00:00   2016-06-09T17:30:00         14         15
    2016-06-09T17:30:00   2017-12-12T16:30:00         15         13
    2017-12-12T16:30:00   2019-12-09T00:00:00         15         14
    2019-12-09T00:00:00   2023-01-04T17:00:00         16         17
    2023-01-04T17:00:00   {t_now}                     16         18
    '''

    with io.StringIO(goes_primaries) as f:
        goes_primary_df = pd.read_table(f, delim_whitespace=True,
                                        parse_dates=True, na_values='None')
    #goes_primary_dict = goes_primary_df.to_dict(orient='records')
    return goes_primary_df
