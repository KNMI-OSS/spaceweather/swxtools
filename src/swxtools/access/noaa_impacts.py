#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""List with 'potential impact' texts for NOAA events."""


def xray_impact(noaa_scale, event_type):
    """Return text describing the impact of the given x-ray event.

        Parameters
        ----------
        noaa_scale : string
            NOAA scale of the event.
        event_type : string
            Event type (alert/summary/warning/watch).

        Returns
        -------
        return_description : string
            Text describing the impact of the x-ray event.
        """

    impact_text = "No impact text available / "

    if 'R2' in noaa_scale:
        if event_type == 'alert':
            impact_text = "Area of impact centered on sub-solar point on the sunlit side of Earth. Extent of blackout of HF (high frequency) radio communication dependent upon current X-ray Flux intensity / "
        elif event_type == 'summary':
            impact_text = "Area of impact centered primarily on sub-solar point on the sunlit side of Earth. Limited blackout of HF (high frequency) radio communication for tens of minutes / "

    elif 'R3' in noaa_scale:
        if event_type == 'alert':
            pass  # TODO, add text for this case
        elif event_type == 'summary':
            impact_text = "Area of impact consists of large portions of the sunlit side of Earth, strongest at the sub-solar point.  Wide area blackout of HF (high frequency) radio communication for about an hour / "

    else:
        # TODO add log message
        print("Unknown short description")

    return impact_text


def radio_impact(short_description, event_type, cancellation):
    """Return text describing the impact of the given radio event.

        Parameters
        ----------
        short_description : string
            Short input description of the event.
        event_type : string
            Event type (alert/summary/warning/watch).
        cancellation : bool
            Indicates whether the message regards a cancelled event.

        Returns
        -------
        return_description : string
            Text describing the impact of the radio event.
        """

    return_description = "No description available"

    if cancellation:
        return_description = "Cancellation of " + short_description

    elif "10cm Radio Burst" in short_description:
        if event_type == 'alert':
            print("No description exists for this short description and event type combination")
        elif event_type == 'summary':
            return_description = "Radio event: 10cm radio burst / A 10 cm radio burst indicates that the electromagnetic burst associated with a solar flare at the 10cm wavelength was double or greater than the initial 10cm radio background. This can be indicative of significant radio noise in association with a solar flare. This noise is generally short-lived but can cause interference for sensitive receivers including radar, GPS, and satellite communications."

    elif "Type II" in short_description:
        if event_type == 'alert':
            return_description = "Radio event: type II radio emmission / Type II emissions occur in association with eruptions on the sun and typically indicate a coronal mass ejection is associated with a flare event."
        elif event_type == 'summary':
            print("No description exists for this short description and event type combination")

    elif "Type IV" in short_description:
        if event_type == 'alert':
            return_description = "Radio event: type IV radio emmission / Type IV emissions occur in association with major eruptions on the sun and are typically associated with strong coronal mass ejections and solar radiation storms."
        elif event_type == 'summary':
            print("No description exists for this short description and event type combination")

    else:
        # TODO add log message
        print("Unknown short description")

    return return_description


def electronflux_impact(event_type):
    """Return text describing the impact of the given electron flux event.

        Parameters
        ----------
        event_type : string
            Event type (alert/summary/warning/watch).

        Returns
        -------
        return_description : string
            Text describing the impact of the electron flux event.
        """

    return_description = "No description available /"

    if event_type == 'alert':
        return_description = "Satellite systems may experience significant charging resulting in increased risk to satellite systems /"

    else:
        # TODO add log message
        print("Unknown short description")

    return return_description


def geomagnetic_impact(event_type, additional_information=()):
    """Return text describing the impact of the given geomagnetic event.

        Parameters
        ----------
        event_type : string
            Event type (alert/summary/warning/watch).
        additional_information : tuple
            A tuple with a range of input variables to define the geomagnetic event.

        Returns
        -------
        return_description : string
            Text describing the impact of the geomagnetic event.
        """

    return_description = "No description available /"

    if event_type == 'alert':
        k_index = additional_information[0]
        return_description = additional_information[1]

        if 'K-index of 4' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            return_description += " Weak power grid fluctuations can occur. Aurora may be visible at high latitudes such as Canada and Alaska."

        elif 'K-index of 5' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            return_description += " Weak power grid fluctuations can occur. Aurora may be visible at high latitudes, i.e., northern tier of the U.S. such as northern Michigan and Maine. Minor impact on satellite operations possible."

        elif 'K-index of 6' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            return_description += " Power grid fluctuations can occur. Aurora may be seen as low as New York to Wisconsin to Washington state. Satellite orientation irregularities may occur. Degraded or episodically blacked radio, small effects on polar HF (high frequency) propagation resulting in fades at lower frequencies."
        elif 'K-index of 7' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            return_description += " Power system voltage irregularities possible, false alarms may be triggered on some protection devices. Systems may experience surface charging; increased drag on low Earth-orbit satellites and orientation problems may occur. Intermittent satellite navigation (GPS) problems, including loss-of-lock and increased range error may occur. HF (high frequency) radio may be intermittent. Aurora may be seen as low as Pennsylvania to Iowa to Oregon."
        elif 'K-index of 8' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            # TODO, still add
            return_description = " Description for K-index of 8 not yet available."
        elif 'K-index of 9' in k_index:
            return_description = k_index + "."
            if len(str(additional_information[1])) > 3:
                return_description += additional_information[1]
            # TODO, still add
            return_description = " Description for K-index of 9 not yet available."
        else:
            if len(str(additional_information[1])) > 3:
                return_description = additional_information[1]

    elif event_type == 'warning':
        warning_type = additional_information[0]
        warning_condition = additional_information[1]

        if warning_type == 'K-index':
            if len(str(additional_information[2])) <= 3:
                return_description = ''
            else:
                return_description = additional_information[2]

            if 'K04' in additional_information[3]:
                return_description = return_description + " Geomagnetic K-index of 4. Weak power grid fluctuations can occur. Aurora may be visible at high latitudes such as Canada and Alaska."
            elif 'K05' in additional_information[3]:
                return_description = return_description + " Geomagnetic K-index of 5. Weak power grid fluctuations can occur. Aurora may be visible at high latitudes, i.e., northern tier of the U.S. such as northern Michigan and Maine. Minor impact on satellite operations possible."
            elif 'K06' in additional_information[3]:
                return_description = return_description + " Geomagnetic K-index of 6. Power grid fluctuations can occur. Aurora may be seen as low as New York to Wisconsin to Washington state. Satellite orientation irregularities may occur."

            if warning_condition == 'persistence':
                return_description = "Peristence of K-index." + return_description
            elif warning_condition == 'onset':
                return_description = "Onset of K-index." + return_description

    elif event_type == 'watch':
        return_description = additional_information[1]

        if 'cancel' in additional_information[0]:
            return_description = additional_information[0]
        if 'G1' in additional_information[0]:
            return_description = additional_information[0] + "." + return_description + " Weak power grid fluctuations can occur. Minor impact on satellite operations possible. Aurora may be visible at high latitudes, i.e., northern tier of the U.S. such as northern Michigan and Maine."
        elif 'G2' in additional_information[0]:
            # TODO why is there no aurora message here?
            return_description = additional_information[0] + "." + return_description + " Power grid fluctuations can occur. Satellite orientation irregularities may occur."

    else:
        print("Unknown event type")

    return return_description


def proton_impact(event_type, swmc=-1):
    """Return text describing the impact of the given proton event.

        Parameters
        ----------
        event_type : string
            Event type (alert/summary/warning/watch).
        swmc : string
            The value of the swmc for the event.

        Returns
        -------
        return_description : string
            Text describing the impact of the proton event.
        """

    return_description = "No description available /"
    if event_type == 'alert' or event_type == 'warning':
        if 'C0' in swmc:
            return_description = "An enhancement in the energetic portion of the solar radiation spectrum may indicate increased biological risk to astronauts or passengers and crew in high latitude, high altitude flights. Additionally, energetic particles may represent an increased risk to all satellite systems susceptible to single event effects. This information should be used in conjunction with the current Solar Radiation Storm conditions when assessing overall impact."
        elif 'X1' in swmc:
            return_description = "Minor impacts on polar HF (high frequency) radio propagation resulting in fades at lower frequencies."
        elif 'X2' in swmc:
            return_description = "Passengers and crew in high latitude, high altitude flights may experience small, increased radiation exposures. Infrequent single-event upsets to satellites are possible. Small effects on polar HF (high frequency) propagation resulting in fades at lower frequencies."
        elif 'X3' in swmc:
            return_description = "Passengers and crew in high latitude, high altitude flights may experience increasing radiation exposures. Astronauts on EVA (extra-vehicular activity) are exposed to elevated radiation levels. Single-event upsets to satellite operations, noise in imaging systems, and slight reduction of efficiency in solar panels are likely. Degraded or episodically blacked-out polar HF (high frequency) radio propagation."

    else:
        print("Unknown event type")

    return return_description
