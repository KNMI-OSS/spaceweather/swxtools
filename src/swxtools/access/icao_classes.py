#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""Define classes for ICAO events to be used during notification processing."""

import logging
import os
import pandas as pd
import numpy as np


# - class ICAOEvent  -------------------------

class ICAOEvent:
    """Define class to initialize an ICAO event consisting of multiple advisories."""

    def __init__(self, settings: dict) -> None:
        """Define ICAO Event object.

        Parameters
        ----------
        settings : dict
           Settings for ICAO event

        """
        self.first_advisory_number = settings['advisory_number']  # Replacement for serial number
        self.effect = settings['effect']  # Replacement for swmc
        self.center = settings['center']
        self.severity = settings['severity']

        time_issued = settings['time_issued']
        self.event_issue_start = pd.to_datetime(time_issued)  # Replacement for issue time
        time_observed = settings['time_observed']
        self.event_observation_start = pd.to_datetime(time_observed)

        self.advisories = self.recursively_extract_event_into_advisories(settings, [])
        # These are ordered in time, but I am not sure whether this is
        # 100% always the case. It is important that they are! (see directly below)

        self.event_issue_end = self.advisories[-1].time_issued
        self.event_observation_end = self.advisories[-1].time_observed

        self.event_issue_duration = self.event_issue_end - self.event_issue_start
        self.event_observation_duration = self.event_observation_end - self.event_observation_start
        self.message = ''

        # Location
        self.location = []
        if 'location_input' in settings:
            lat_coordinates = settings['location_input'][0:-1]
            lon_coordinates = settings['location_input'][-1]
            while len(lon_coordinates) > 0:
                lat_0 = lat_coordinates[0]
                lat_1 = lat_coordinates[1]
                lon_0 = lon_coordinates.pop(0)
                lon_1 = lon_coordinates.pop(0)
                polygon = [[lat_0, lon_0], [lat_1, lon_0], [lat_1, lon_1], [lat_0, lon_1]]
                self.location.append(polygon)
            # print("LOCATION", self.location)
            # (-180.0, 180.0, [60, 90, -90, -60])
        else:
            self.location = []

        # event_dict['location_input'] = icao_parser.parse_location_code(event_dict['obs_swx_text'])

        # Added elements
        self.center_country = settings['center_country']
        self.observation_text = settings['obs_swx_text']
        self.forecast_6h_text = settings['forecast_6h_text']
        self.forecast_6h_date = settings['forecast_6h_date']
        self.forecast_12h_text = settings['forecast_12h_text']
        self.forecast_12h_date = settings['forecast_12h_date']
        self.forecast_18h_text = settings['forecast_18h_text']
        self.forecast_18h_date = settings['forecast_18h_date']
        self.forecast_24h_text = settings['forecast_24h_text']
        self.forecast_24h_date = settings['forecast_24h_date']
        self.replacement_number = settings['nr_replacement']
        self.remark = settings['remark']
        self.next_advisory = settings['next_advisory']
        self.number_of_replacements = settings['num_replacements']
        self.replacements = settings['replacements']

    def recursively_extract_event_into_advisories(self, event_dict, advisory_list):
        """Extract advisories from an event.

        Parameters
        ----------
        # TODO

        """
        newAdvisory = ICAOAdvisory(event_dict)
        advisory_list.append(newAdvisory)
        if event_dict['num_replacements'] > 0:
            for sub_dict in event_dict['replacements']:
                advisory_list = self.recursively_extract_event_into_advisories(sub_dict, advisory_list)
        return advisory_list

    def generate_message(self):
        # TODO finish if needed
        return self.remark


class ICAOAdvisory():
    def __init__(self, advisory_dict):
        self.advisory_number = advisory_dict['advisory_number']
        self.effect = advisory_dict['effect']
        self.center = advisory_dict['center']
        self.severity = advisory_dict['severity']
        self.nr_replacement = advisory_dict['nr_replacement']
        self.next_advisory = advisory_dict['next_advisory']
        self.obs_swx_text = advisory_dict['obs_swx_text']
        self.remark = advisory_dict['remark']
        self.forecast_6h_date = advisory_dict['forecast_6h_date']
        self.forecast_12h_date = advisory_dict['forecast_12h_date']
        self.forecast_18h_date = advisory_dict['forecast_18h_date']
        self.forecast_24h_date = advisory_dict['forecast_24h_date']
        self.forecast_6h_text = advisory_dict['forecast_6h_text']
        self.forecast_12h_text = advisory_dict['forecast_12h_text']
        self.forecast_18h_text = advisory_dict['forecast_18h_text']
        self.forecast_24h_text = advisory_dict['forecast_24h_text']

        self.center_country = advisory_dict['center_country']
        subcenters = {'YMMC': 'Commonwealth of Australia',
                      'LFPW': 'French Republic',
                      'ZBBB': 'People\'s Republic of China',
                      'UUAG': 'Russian Federation',
                      'EFKL': 'Republic of Finland',
                      'EGRR': 'United Kingdom of Great Britain and Northern Ireland',
                      'KWNP': 'United States of America'}
        self.center_country_text = subcenters[self.center_country]

        time_issued = advisory_dict['time_issued']
        self.time_issued = pd.to_datetime(time_issued)
        time_observed = advisory_dict['time_observed']
        self.time_observed = pd.to_datetime(time_observed)
        self.latency = self.time_issued - self.time_observed
