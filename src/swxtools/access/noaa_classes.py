#
# This file is part of hapi-ingest-scripts
#
# https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts
#
# Copyright (c) 2024 KNMI
# All Rights Reserved
"""Define classes for NOAA events to be used during notification processing."""

import numpy as np
from noaa_impacts import xray_impact, radio_impact, \
    electronflux_impact, geomagnetic_impact, proton_impact


# - class NOAAEvent  -------------------------
class NOAAEvent:
    """Define class to initialize a NOAA event."""

    def __init__(self, settings: dict) -> None:
        """Define NOAA Event object.

            Parameters
            ----------
            settings : dict
               Settings for NOAA event

            """
        # initialize settings for this algorithm
        self.swmc = settings["swmc"]  # Space Weather Message Code
        self.serial_number = settings["serial_number"]
        self.original_message = settings["original_message"]
        self.issue_time = settings["issue_time"].strip()
        self.event_type = settings["event_type"].strip()


class XRayEvent(NOAAEvent):
    """Define X-ray event."""

    def __init__(self, settings: dict):
        """Define NOAA X-Ray Event object.

            Parameters
            ----------
            settings : dict
               Settings for X-ray event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Basic properties
        self.potential_impacts = np.nan
        self.additional_information = np.nan

        # Event-type specific properties
        if self.event_type == 'alert':
            self.short_description = settings["alert"]
            if 'cancel_serial_number' in settings:
                self.cancel_serial_number = settings["cancel_serial_number"]
            else:
                self.noaa_scale = settings["noaa_scale"]
                self.threshold_reached = settings["threshold_reached"].strip()
        elif self.event_type == 'summary':
            self.short_description = settings["summary"]
            if 'cancel_serial_number' in settings:
                self.cancel_serial_number = settings["cancel_serial_number"]
            else:
                self.begin_time = settings["begin_time"].strip()
                self.maximum_time = settings["maximum_time"].strip()
                self.end_time = settings["end_time"].strip()
            if 'location' in settings:
                self.location = settings["location"].strip()
            if 'xray_class' in settings:
                self.xray_class = settings["xray_class"].strip()
            if 'optical_class' in settings:
                self.optical_class = settings["optical_class"]
            if 'noaa_scale' in settings:
                self.noaa_scale = settings["noaa_scale"]
        else:
            print("Unknown event type")
            # Note: x-ray events have no warnings and watches

    def generate_message(self):
        """Set value of the additional information variable with a summary of the event."""

        # Search for potential NOAA scale impact description
        cancelled = False
        self.potential_impacts = ''
        if hasattr(self, "noaa_scale") and len(str(self.noaa_scale)) > 3:
            self.potential_impacts = xray_impact(self.noaa_scale, self.event_type)

        if self.event_type == 'alert':
            if cancelled:
                self.additional_information = "Called alert: " + self.short_description
            else:
                self.additional_information = self.short_description
                if hasattr(self, "noaa_scale"):
                    self.additional_information += " / NOAA scale: " + str(self.noaa_scale)
                if hasattr(self, "threshold_reached"):
                    self.additional_information += " / Threshold reached at: " + str(self.threshold_reached)
        elif self.event_type == 'summary':
            if cancelled:
                self.additional_information = "Called summary: " + self.short_description
            else:
                self.additional_information = self.short_description
                if hasattr(self, "noaa_scale"):
                    self.additional_information += " / NOAA scale: " + str(self.noaa_scale)
                if hasattr(self, "xray_class"):
                    self.additional_information += " / X-ray class: " + str(self.xray_class)
                if hasattr(self, "location"):
                    self.additional_information += " / At location: " + str(self.location)
                if hasattr(self, "begin_time") and hasattr(self, "end_time"):
                    self.additional_information += " / Started at " + str(self.begin_time) + \
                                                   " and lasted until " + str(self.end_time)
                if hasattr(self, "maximum_time"):
                    self.additional_information += " / Maximum intensity at " + str(self.maximum_time)
            # Not included: optical class


class RadioEvent(NOAAEvent):
    """Define Radio event."""

    def __init__(self, settings: dict):
        """Define NOAA Event object.

            Parameters
            ----------
            settings : dict
               Settings for radio event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Basic properties
        self.description = np.nan

        # Event-type specific properties
        if self.event_type == 'alert':
            if 'cancel_serial_number' in settings:
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.short_description = settings["alert"]
            else:
                self.begin_time = settings["begin_time"].strip()
                self.short_description = settings["alert"]
                self.estimated_velocity = settings["estimated_velocity"]
        elif self.event_type == 'summary':
            if 'cancel_serial_number' in settings:
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.short_description = settings["summary"]
            else:
                self.begin_time = settings["begin_time"].strip()
                self.short_description = settings["summary"]
                self.maximum_time = settings["maximum_time"]
                self.end_time = settings["end_time"]
                self.duration = settings["duration"]
                self.peak_flux = settings["peak_flux"]
                self.latest_penticton_noon_flux = settings["latest_penticton_noon_flux"]

        else:
            print("Unknown event type")
            # Note: radio events have no warnings and watches

    def generate_message(self):
        """Set value of the description variable with a summary of the event."""
        if hasattr(self, "cancel_serial_number"):
            self.description = radio_impact(self.short_description, self.event_type, True)
        else:
            self.description = radio_impact(self.short_description, self.event_type, False)


class ProtonEvent(NOAAEvent):
    """Define Radio event."""

    def __init__(self, settings: dict):
        """Define NOAA Proton object.

            Parameters
            ----------
            settings : dict
               Settings for proton event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Basic properties
        self.description = np.nan

        # Event-type specific properties
        if self.event_type == 'alert':
            if "cancel_serial_number" in settings:
                self.short_description = settings["alert"]
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.begin_time = np.nan
            elif "ALTPC" in self.swmc:
                self.short_description = settings["alert"]
                self.begin_time = settings["begin_time"]
            elif "ALTPX" in self.swmc:
                self.short_description = settings["alert"]
                self.begin_time = settings["begin_time"]
                self.noaa_scale = settings["noaa_scale"]

        elif self.event_type == 'summary':
            if "cancel_serial_number" in settings:
                self.short_description = settings["summary"]
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.begin_time = np.nan
            elif "SUMPC" in self.swmc:
                self.short_description = settings["summary"]
                self.begin_time = settings["begin_time"]
                self.maximum_time = settings["maximum_time"]
                self.end_time = settings["end_time"]
                self.maximum_100mev_flux = settings["maximum_100mev_flux"]
            elif "SUMPX" in self.swmc:
                self.short_description = settings["summary"]
                self.begin_time = settings["begin_time"]
                self.maximum_time = settings["maximum_time"]
                self.end_time = settings["end_time"]
                self.maximum_10mev_flux = settings["maximum_10mev_flux"]
                self.noaa_scale = settings["noaa_scale"]

        elif self.event_type == 'warning':
            self.short_description = settings["warning"]
            self.warning_condition = settings["warning_condition"]
            if "WARPC" in self.swmc:
                if 'Onset' in settings["warning_condition"]:
                    self.short_description = settings["warning"]
                    self.valid_to = settings["valid_to"]
                    self.valid_from = settings["valid_from"]
                elif 'ersistence' in settings["warning_condition"]:
                    self.short_description = settings["warning"]
                    self.valid_to = settings["valid_to"]
                    self.valid_from = settings["valid_from"]
                    self.extension_serial_number = settings["extension_serial_number"]
                else:
                    self.short_description = settings["warning"]
                    self.cancel_serial_number = settings["cancel_serial_number"]
            elif "WARPX" in self.swmc:
                if 'Onset' in settings["warning_condition"]:
                    self.short_description = settings["warning"]
                    self.valid_to = settings["valid_to"]
                    self.valid_from = settings["valid_from"]
                    self.noaa_scale_predicted = settings["noaa_scale_predicted"]
                elif 'ersistence' in settings["warning_condition"]:
                    self.short_description = settings["warning"]
                    self.valid_to = settings["valid_to"]
                    self.valid_from = settings["valid_from"]
                    self.noaa_scale_predicted = settings["noaa_scale_predicted"]
                    self.extension_serial_number = settings["extension_serial_number"]
                else:
                    self.short_description = settings["warning"]
                    self.cancel_serial_number = settings["cancel_serial_number"]

            if "extension_serial_number" in settings:
                self.extension_serial_number = settings["extension_serial_number"]

        else:
            print("Unknown event type")
            # Note: proton events only have alerts, summaries and warnings

    def generate_message(self):
        """Set value of the description variable with a summary of the event."""
        if self.event_type == 'alert':
            self.description = proton_impact(self.event_type, swmc=self.swmc)
            if len(str(self.begin_time)) <= 3:
                self.short_description = "Conditions for this alert are no longer present."
                self.description = self.short_description + ". Cancelled serial number " + str(int(self.cancel_serial_number))
            elif "ALTPC" in self.swmc:
                if len(str(self.short_description)) <= 3:
                    self.short_description = "Proton event"
                self.description = self.short_description + " starting at " + self.begin_time + ". " + self.description

            elif "ALTPX" in self.swmc:
                if len(str(self.short_description)) <= 3:
                    self.short_description = "Proton event"
                if len(str(self.noaa_scale)) > 3:
                    self.description = self.short_description + " starting at " + self.begin_time + ". NOAA scale: " + self.noaa_scale + ". " + self.description
                else:
                    self.description = self.short_description + " starting at " + self.begin_time + ". " + self.description

        if self.event_type == 'summary':
            if len(str(self.begin_time)) <= 3:
                self.short_description = "Conditions for this summary are no longer present."
                self.description = self.short_description + " Cancelled serial number " + str(int(self.cancel_serial_number))
            elif "SUMPC" in self.swmc:
                self.description = self.short_description + " starting at " + self.begin_time + " until " + self.end_time + " with a maximum time of " + self.maximum_time + ". Maximum 100MeV flux: " + self.maximum_100mev_flux
            elif "SUMPX" in self.swmc:
                if len(str(self.noaa_scale)) > 3:
                    self.description = self.short_description + " starting at " + self.begin_time + " until " + self.end_time + " with a maximum time of " + self.maximum_time + " . NOAA scale: " + self.noaa_scale + ". Maximum 10MeV flux: " + self.maximum_10mev_flux
                else:
                    self.description = self.short_description + " starting at " + self.begin_time + " until " + self.end_time + " with a maximum time of " + self.maximum_time + " . Maximum 10MeV flux: " + self.maximum_10mev_flux

        if self.event_type == 'warning':
            self.description = proton_impact(self.event_type, swmc=self.swmc)
            if "WARPC" in self.swmc:
                if 'Onset' in self.warning_condition:
                    if len(str(self.valid_to)) <= 3:
                        self.description = self.short_description + " (onset)" + " valid from " + self.valid_from + ". " + self.description
                    else:
                        self.description = self.short_description + " (onset)" + " valid from " + self.valid_from + " until " + self.valid_to + ". " + self.description
                elif 'ersistence' in self.warning_condition:
                    if len(str(self.extension_serial_number)) <= 3:
                        self.description = self.short_description + " valid from " + self.valid_from + " and now valid until " + self.valid_to + ". " + self.description
                    else:
                        self.description = self.short_description + " (persistence of " + str(int(self.extension_serial_number)) + ")" + " valid from" + self.valid_from + " and now valid until " + self.valid_to + ". " + self.description
                else:
                    if len(str(self.short_description)) <= 3:
                        self.short_description = "Conditions for this warning are no longer present."
                    self.description = self.short_description + " Cancelled serial number " + str(int(self.cancel_serial_number))

            elif "WARPX" in self.swmc:
                if 'Onset' in self.warning_condition:
                    if len(str(self.valid_to)) <= 3:
                        self.description = self.short_description + " (onset)" + " valid from " + self.valid_from + ". " + self.description
                    else:
                        self.description = self.short_description + " (onset)" + " valid from " + self.valid_from + " until " + self.valid_to + ". " + self.description
                    if len(str(self.noaa_scale_predicted)) > 3:
                        self.description += " Predicted NOAA scale: " + str(self.noaa_scale_predicted)
                elif 'ersistence' in self.warning_condition:
                    if len(str(self.extension_serial_number)) <= 3:
                        self.description = self.short_description + " valid from " + str(
                            self.valid_from) + " and now valid until " + str(
                            self.valid_to) + ". " + self.description
                    else:
                        self.description = self.short_description + " (persistence of " + str(int(self.extension_serial_number)) + ")" + " valid from " + str(self.valid_from) + " and now valid until " + str(self.valid_to) + ". " + self.description
                    if len(str(self.noaa_scale_predicted)) > 3:
                        self.description += " Predicted NOAA scale: " + str(self.noaa_scale_predicted)
                else:
                    if len(str(self.short_description)) <= 3:
                        self.short_description = "Conditions for this warning are no longer present."
                    self.description = self.short_description + " Cancelled serial number " + str(int(self.cancel_serial_number))


class ElectronFluxEvent(NOAAEvent):
    """Define electron-flux event."""

    def __init__(self, settings: dict):
        """Define NOAA Electron Flux object.

            Parameters
            ----------
            settings : dict
               Settings for electron-flux event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Basic properties
        self.potential_impacts = np.nan
        self.additional_information = np.nan
        self.continued = settings["continued"]

        # Event-type specific properties
        if self.event_type == 'alert':
            # Continued alert
            if self.continued:
                self.short_description = settings["continued_alert"]
                self.continuation_serial_number = settings["continuation_serial_number"]
                self.begin_time = settings["begin_time"]
                self.yesterday_max_flux = settings["yesterday_max_flux"]
            # Initial alert
            else:
                self.short_description = settings["alert"]
                self.threshold_reached = settings["threshold_reached"]
                self.station = settings["station"]
        else:
            print("Unknown event type")
            # Note: electron flux events have only alerts

    def generate_message(self):
        """Set value of the additional information variable with a summary of the event."""
        self.potential_impacts = electronflux_impact(self.event_type)
        if self.continued:
            if len(str(self.begin_time)) > 3:
                self.additional_information = " Continuation of serial number " + str(int(self.continuation_serial_number)) + \
                                                  " / Begin time: " + str(self.begin_time) + \
                                                  " / Yesterday's maximum flux: " + str(self.yesterday_max_flux)
            else:
                self.begin_time = ''
                self.additional_information = " Continuation of serial number " + str(int(self.continuation_serial_number)) + \
                                              " / Yesterday's maximum flux: " + str(self.yesterday_max_flux)
        else:
            self.additional_information = " Initial alert"
            if len(str(self.threshold_reached)) > 3:
                self.additional_information += " with the threshold reached at " + str(self.threshold_reached)
            if len(str(self.station)) > 3:
                self.additional_information += " at station " + str(self.station)


class GeomagneticEvent(NOAAEvent):
    """Define Geomagnetic event."""

    def __init__(self, settings: dict):
        """Define NOAA Geomagnetic object.

            Parameters
            ----------
            settings : dict
               Settings for geomagnetic event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Basic properties
        self.description = np.nan

        # Event-type specific properties
        if self.event_type == 'alert':
            if 'cancel_serial_number' in settings:
                self.short_description = settings["alert"]
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.comment = settings["comment"]
            else:
                self.short_description = settings["alert"]
                self.noaa_scale = settings["noaa_scale"]
                if len(str(self.noaa_scale)) < 4:
                    self.noaa_scale = "Not available"
                self.potential_impacts_base = settings["potential_impacts_base"]
                self.threshold_reached = settings["threshold_reached"]
                self.synoptic_period = settings["synoptic_period"]
                self.active_warning = settings["active_warning"]

        elif self.event_type == 'summary':
            if 'cancel_serial_number' in settings:
                self.short_description = settings["summary"]
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.comment = settings["comment"]
            else:
                self.short_description = settings["summary"]
                self.observed = settings["observed"]
                self.deviation = settings["deviation"]
                self.station = settings["station"]

        elif self.event_type == 'warning':
            if "K0" in self.swmc:
                # K-index event

                if "extended_warning" in settings:
                    if 'cancel_serial_number' in settings:
                        self.short_description = settings["warning"]
                        self.cancel_serial_number = settings["cancel_serial_number"]
                        self.comment = settings["comment"]
                    else:
                        # Persistence warning
                        self.short_description = settings["warning"]
                        self.geomagnetic_event = settings["geomagnetic_event"]
                        self.valid_from = settings["valid_from"]
                        self.valid_to = settings["valid_to"]
                        self.noaa_scale = settings["noaa_scale"]
                        self.potential_impacts_base = settings["potential_impacts_base"]
                        self.warning_condition = "persistence"
                else:
                    if 'cancel_serial_number' in settings:
                        self.short_description = settings["warning"]
                        self.cancel_serial_number = settings["cancel_serial_number"]
                        self.comment = settings["comment"]
                    else:
                        # Initial warning
                        self.short_description = settings["warning"]
                        self.geomagnetic_event = settings["geomagnetic_event"]
                        self.valid_from = settings["valid_from"]
                        self.valid_to = settings["valid_to"]
                        self.noaa_scale = settings["noaa_scale"]
                        self.potential_impacts_base = settings["potential_impacts_base"]
                        self.warning_condition = "onset"

            elif "SUD" in self.swmc:
                # Sudden impulse event

                if "cancel_serial_number" in settings:
                    # Cancellation of warning
                    self.short_description = settings["warning"]
                    self.geomagnetic_event = settings["geomagnetic_event"]
                    self.comment = settings["comment"]
                    self.cancel_serial_number = settings["cancel_serial_number"]
                    self.warning_condition = "cancel" # TODO do we need this
                else:
                    # Regular warning
                    self.short_description = settings["warning"]
                    self.geomagnetic_event = settings["geomagnetic_event"]
                    self.valid_from = settings["valid_from"]
                    self.valid_to = settings["valid_to"]
                    self.ip_shock_passage = settings["ip_shock_passage"]
                    self.warning_condition = "regular"

        elif self.event_type == 'watch':
            self.short_description = settings["watch"]
            self.potential_impacts_base = settings["potential_impacts_base"]
            if "cancel_serial_number" in settings:
                self.comment = settings["comment"]
                self.cancel_serial_number = settings["cancel_serial_number"]
                self.short_description = "Geomagnetic watch from serial number " + \
                                         str(int(self.cancel_serial_number)) + " cancelled."

        else:
            print("Unknown event type")

    def generate_message(self):
        """Set value of the descruption variable with a summary of the event."""
        if self.event_type == 'alert':
            # Cancelled alert
            if hasattr(self, "cancel_serial_number"):
                self.description = 'Cancelled alert with description: ' + self.short_description
                if len(str(self.comment)) > 3:
                    self.description += " / Reason: " + self.comment
                self.description += " / Cancelled serial number: " + str(int(self.cancel_serial_number))

            # Regular alert
            else:
                self.description = geomagnetic_impact(self.event_type, additional_information=(self.short_description, self.potential_impacts_base, self.swmc))
                if len(str(self.threshold_reached)) > 3:
                    self.description += " / Threshold reached at" + self.threshold_reached
                if len(str(self.synoptic_period)) > 3:
                    self.description += " / Synoptic period of" + self.synoptic_period
                if 'available' not in self.noaa_scale:
                    self.description = self.description + " NOAA scale:" + self.noaa_scale

        elif self.event_type == 'summary':
            # Cancelled summary
            if hasattr(self, "cancel_serial_number"):
                self.description = 'Cancelled summary with description: ' + self.short_description
                if len(str(self.comment)) > 3:
                    self.description += " / Reason: " + self.comment
                self.description += " / Cancelled serial number: " + str(int(self.cancel_serial_number))

            # Regular summary
            else:
                self.description = "Summary of geomagnetic sudden impulse. Observed at station " + self.station + " at " + self.observed + " with a deviation of " + self.deviation + "."

        elif self.event_type == 'warning':
            if "K0" in self.swmc:
                # Cancelled K0 warning
                if hasattr(self, "cancel_serial_number"):
                    self.description = ''
                    self.description = 'Cancelled K0 warning '
                    if len(str(self.short_description)) > 3:
                        self.description += ' with description: ' + self.short_description
                    if len(str(self.comment)) > 3:
                        self.description += " / Reason: " + self.comment
                    self.description += " / Cancelled serial number: " + str(int(self.cancel_serial_number))

                # Regular K0 warning
                else:
                    self.description = geomagnetic_impact(self.event_type,
                                                          additional_information=("K-index", self.warning_condition, self.potential_impacts_base, self.swmc))
            elif "SUD" in self.swmc:
                # Cancelled sudden impulse warning
                if hasattr(self, "cancel_serial_number"):
                    self.description = 'Cancelled sudden impulse warning with description: ' + self.short_description
                    if len(str(self.comment)) > 3:
                        self.description += " / Reason: " + self.comment
                    self.description += " / Cancelled serial number: " + str(int(self.cancel_serial_number))

                # Regular sudden impulse warning
                else:
                    self.description = self.short_description + ". Valid from " + self.valid_from + " to " + self.valid_to + "."
                    if len(str(self.ip_shock_passage)) > 3:
                        self.description += " IP Shock Passage observed at " + self.ip_shock_passage + "."

        elif self.event_type == 'watch':
            # Cancelled watch
            if hasattr(self, "cancel_serial_number"):
                self.description = 'Cancelled watch with description: ' + self.short_description
                if len(str(self.comment)) > 3:
                    self.description += " / Reason: " + self.comment
                self.description += " / Cancelled serial number: " + str(int(self.cancel_serial_number))

            # Regular watch
            else:
                self.description = geomagnetic_impact(self.event_type, additional_information=(self.short_description, self.potential_impacts_base))

        else:
            print("Unknown event type")


class StratosphericEvent(NOAAEvent):
    """Define Stratospheric event."""

    def __init__(self, settings: dict):
        """Define NOAA Stratospheric object.

            Parameters
            ----------
            settings : dict
               Settings for stratospheric event.

            """

        # Inherit from NOAAEvent
        super().__init__(settings)

        # Event-type specific properties
        if self.event_type == 'alert':
            self.utc_day = settings["utc_day"]
            self.comment = 'Stratospheric warning ongoing, valid for UTC day ' + settings["utc_day"]
            # Note: no processing required, self.comment is the message in the timeline viewer
        else:
            print("Unknown event type")
            # Note: stratospheric events have only alerts
