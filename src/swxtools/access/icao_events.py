import os
import io
import re
import requests
import time
import numpy as np
import pandas as pd
import netCDF4
import logging
import json
import swxtools.access.icao_parser as icao_parser
from math import *
from bs4 import BeautifulSoup
from swxtools import download_tools
from datetime import datetime as dt
from swxtools.config import config
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from os import walk
import zipfile
from swxtools.access.icao_classes import ICAOEvent, ICAOAdvisory

# - global parameters ------------------------------
hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

# Time format for timeline viewer
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"


# Not implemented
def download_icao_html(local_dir='/icao_events/'):
    """Download ICAO archive html files from server

    Parameters
    ----------
    local_dir : string
        A string indicating the local directory to save the html files to.

    Returns
    -------
    None

    Saves the archive files in the local_dir.
    """

    # TODO no ICAO archive data exist yet


# Not implemented
def download_icao_recent_json() -> None:
    """Download recent ICAO events from online json files to a Python dictionary

    Parameters
    ----------
    None

    Returns
    -------
    notifications : dict
        A dictionary with all recent NOAA events.
    """

    # TODO no recent ICAO data exists yet
    return None


# Not implemented
def parse_icao_html_to_dict(html_input_list: list = -1, parser_data: dict = -1) -> list:
    """Read ICAO html files and process to a list with dictionaries

    Parameters
    ----------
    html_input_list : list
        A list with strings. Each string contains the path
        to an archival alerts ICAO html file.
    parser_data : dict
        A dictionary with the settings from the argument parser.

    Returns
    -------
    A list with initial dictionaries of ICAO events
    """

    # TODO no ICAO html data parsed yet
    return None


# Not implemented
def parse_icao_recent_json_to_dict(notifications: dict = -1, parser_data: dict = -1) -> list:
    """Read recent ICAO data from dictionary and process to a list with dictionaries

    Parameters
    ----------
    notifications : dict
        A dictionary with all the notifications in the recent json file.
    parser_data : dict
        A dictionary with the settings from the argument parser.

    Returns
    -------
    A list with initial dictionaries of ICAO events
    """

    # TODO no recent ICAO data is parsed yet
    return []


def parse_icao_csv_to_dict(csv_list: list = -1, parser_data: dict = -1) -> list:
    """Read ICAO data from csv files and process to initial dictionary

    Parameters
    ----------
    csv_list : list
        A list with strings containing the path to csv files with ICAO events.
    parser_data : dict
        A dictionary with the settings from the argument parser.

    Returns
    -------
    A list with initial dictionaries of ICAO events
    """

    icao_list = []

    for csv_fname in csv_list:

        # Load csv file into pandas dataframe
        df_full = pd.read_csv(csv_fname, keep_default_na=False)

        # Ignore advisories if status field = TEST or TEST is mentioned in the remarks field
        df_notest = df_full[(df_full['STATUS'] != 'TEST') & (~df_full['RMK'].str.contains('TEST'))]

        # Make a subset of only 'starting advisories' (so without advisories that
        # replace another one)
        df_only_opening_advisories = df_notest[df_notest['NR RPLC'] == 'VOID']

        # Loop over all 'opening advisories', parse them, combine associated advisories into events
        # and put them in a list.
        for advisory_dict in df_only_opening_advisories.to_dict(orient='records'):
            try:
                # Subsequent advisories that replace previous ones are handled in the
                # parsing method and are included as nested dictionaries. In order to
                # find advisories replacing an original one, the whole set needs to be
                # passed to the parsing method as well (df_notest)
                event_dict = icao_parser.parse_advisory_and_combine_into_events(advisory_dict, df_notest)
                event_dict['location_input'] = icao_parser.parse_location_code(event_dict['obs_swx_text'])

                icao_object = ICAOEvent(event_dict)
                icao_list.append(icao_object)
            except ValueError:
                # logging.error(f"Could not parse advisory with number {record['ADVISORY NR']}")
                print('Value error, advisory: ', advisory_dict)

    return icao_list


def convert_icao_gnss(dict_list: list = -1) -> list:
    """Convert ICAO dictionary list input to upload-ready GNSS events

    Parameters
    ----------
    dict_list : list
        A list with parsed dictionary entries for ICAO events.

    Returns
    -------
    A list with dictionaries. Each dictionary consists of
    upload-ready information about one event GNSS event.
    """

    gnss_notifications = []

    # Loop over all icao events in the list
    for event in dict_list:
        # Check whether event has gnss type
        if 'GNSS' in event.effect:
            # Generate gnss event object
            gnss_timeline_viewer_entry = {'product_id': event.first_advisory_number.strip(),
                                          'event_type': 'GNSS',
                                          'message_code': 'GNSS_' + event.severity,
                                          'issue_datetime': event.event_issue_start,
                                          'begin_time': event.event_issue_start,
                                          'end_time': event.event_issue_end,
                                          'severity': event.severity,
                                          'message': event.remark,
                                          'coordinate_set': str(event.location)}

            gnss_notifications.append(gnss_timeline_viewer_entry)

    return gnss_notifications


def convert_icao_hf(dict_list: list = -1) -> list:
    """Convert ICAO dictionary list input to upload-ready HF COM events

    Parameters
    ----------
    dict_list : list
        A list with parsed dictionary entries for ICAO events.

    Returns
    -------
    A list with dictionaries. Each dictionary consists of
    upload-ready information about one HF COM event.
    """

    hfcom_notifications = []

    # Loop over all icao events in the list
    for event in dict_list:
        # Check whether event has gnss type
        if 'HF' in event.effect:
            # Generate hf com event object
            hfcom_timeline_viewer_entry = {'product_id': event.first_advisory_number.strip(),
                                           'event_type': 'HF',
                                           'message_code': 'HF_' + event.severity,
                                           'issue_datetime': event.event_issue_start,
                                           'begin_time': event.event_issue_start,
                                           'end_time': event.event_issue_end,
                                           'severity': event.severity,
                                           'message': event.remark,
                                           'coordinate_set': str(event.location)}

            hfcom_notifications.append(hfcom_timeline_viewer_entry)

    return hfcom_notifications


def convert_icao_rad(dict_list: list = -1) -> list:
    """Convert ICAO dictionary list input to upload-ready RADIATION events

    Parameters
    ----------
    dict_list : list
        A list with parsed dictionary entries for ICAO events.

    Returns
    -------
    A list with dictionaries. Each dictionary consists of
    upload-ready information about one RADIATION event.
    """

    rad_notifications = []

    # Loop over all icao events in the list
    for event in dict_list:
        # Check whether event has gnss type
        if 'RAD' in event.effect:
            # Generate radiation event object
            rad_timeline_viewer_entry = {'product_id': event.first_advisory_number.strip(),
                                         'event_type': 'RAD',
                                         'message_code': 'RAD_' + event.severity,
                                         'issue_datetime': event.event_issue_start,
                                         'begin_time': event.event_issue_start,
                                         'end_time': event.event_issue_end,
                                         'severity': event.severity,
                                         'message': event.remark,
                                         'coordinate_set': str(event.location)}

            rad_notifications.append(rad_timeline_viewer_entry)

    return rad_notifications


# Temporary method to test directly from swxtools, commented for now
if __name__ == "__main__":

    # Download
    print("Start downloading")
    # download_icao_html()
    # recent_notifications = download_icao_recent_json()
    print("Finished downloading")

    # Make list of html files
    # read_list = []
    # path = config['local_source_data_path'] + '/icao_events'

    # for root, dirc, files in walk(path):
    #     for FileName in files:
    #         if '.zip' not in FileName:
    #             read_list.append(path + '/' + FileName)

    # Parse
    print("Start parsing")
    # archive_dataframes = parse_icao_html_to_dataframe(read_list)
    # recent_dataframes = parse_icao_recent_json_to_dataframe(recent_notifications)
    csv_dict = parse_icao_csv_to_dict(['icao_advisories.csv'])
    print("Finished parsing")

    # print("Archive dataframes", archive_dataframes)
    # print("Recent dataframes", recent_dataframes)
    # print("Csv dict", csv_dict)
    # for item in csv_dataframes:
    #     print(item)
    #     print('')

    # Convert
    print("Start converting")
    gnss_notifications_final = convert_icao_gnss(csv_dict)
    hfcom_notifications_final = convert_icao_hf(csv_dict)
    rad_notifications_final = convert_icao_rad(csv_dict)
    print("Finished converting")

    # Print results
    print("Print results")
    print("GNSS events", gnss_notifications_final)
    print('')
    print("HF COM events", hfcom_notifications_final)
    print('')
    print("RADIATION events", rad_notifications_final)
    print("Finished printing results")
