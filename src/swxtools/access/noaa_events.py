import os
import io
import re
import requests
import time
import numpy as np
import pandas as pd
import netCDF4
import logging
import json
from math import *
from bs4 import BeautifulSoup
from swxtools import download_tools
from datetime import datetime as dt
from swxtools.config import config
from swxtools.hapi_client import (
    get_info_values,
    ensure_dataset_info,
    add_data,
    resample_lower_cadences,
)
from os import walk
import zipfile
from noaa_classes import NOAAEvent, ElectronFluxEvent, GeomagneticEvent, ProtonEvent, \
    RadioEvent, StratosphericEvent, XRayEvent

# - global parameters ------------------------------
hapi_server = config['hapi_server']
hapi_server_key = config['hapi_server_key']

# Time format for timeline viewer
ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"


def download_noaa_html(local_dir='/noaa_events/'):
    """Download NOAA archive html files from ftp server

    Parameters
    ----------
    local_dir : string
        A string indicating the local directory to save the html files.

    Returns
    -------
    None

    Saves the archive files in the local_dir.
    """

    base_path = config['local_source_data_path']

    # Inputs for the mirror function
    base_url = 'ftp://ftp.swpc.noaa.gov/pub/alerts/'
    sub_url = ''

    # Mirror function
    # First check whether archive has already been downloaded
    if 'alerts_201501.html' not in os.listdir(base_path + local_dir):
        downloaded_files = download_tools.mirror(base_url, sub_url, base_path + local_dir)
    else:
        print('Archive already downloaded')

    # Check whether zip files have already been unzipped
    if 'alerts_200201.html' in os.listdir(base_path + local_dir):
        unpacked = True
    else:
        unpacked = False

    if not unpacked:
        # Unpack all zip files
        for filename in os.listdir(base_path + local_dir):
            if '.zip' in filename:
                with zipfile.ZipFile(base_path + local_dir + filename, 'r') as zip_ref:
                    zip_ref.extractall(base_path + local_dir)
    else:
        print('Zip files already unpacked')

    for filename in os.listdir(base_path + local_dir):
        # Remove all png files and archive websites (new loop since unpacked files are included)
        if '.png' in filename or 'archive_' in filename:
            os.remove(base_path + local_dir + filename)


def download_noaa_recent_json() -> dict:
    """Download recent NOAA events from online json file to a Python dictionary

    Parameters
    ----------
    None

    Returns
    -------
    notifications : dict
        A dictionary with all recent NOAA events.
    """

    # Retrieve alerts and put in notifications object
    url = 'https://services.swpc.noaa.gov/products/alerts.json'
    r = requests.get(url)
    if r.ok:
        json_text = r.content
        notifications = json.loads(json_text)
        return notifications
    else:
        print(f"Error getting {url}")
        return {}


def parse_noaa_html_to_dataframe(html_input_list: list = -1, parser_data: dict = -1) -> dict:
    """Read NOAA html files and process to initial dataframes

    Parameters
    ----------
    html_input_list : list
        A list with strings. Each string contains the path
        to an archival alerts NOAA html file.
    parser_data : dict
        A dictionary with the settings from the argument parser.

    Returns
    -------
    A dictionary with dataframes with events for the following event types
        * Electron-flux events
        * Geomagnetic events
        * Proton events
        * Radio events
        * Stratospheric events
        * X-ray events
    """

    noaa_events = []  # list with all events
    noaa_electronflux = {"Space Weather Message Code": [], "Original Message": []}
    noaa_geomagnetic = {"Space Weather Message Code": [], "Original Message": []}
    noaa_proton = {"Space Weather Message Code": [], "Original Message": []}
    noaa_radio = {"Space Weather Message Code": [], "Original Message": []}
    noaa_stratospheric = {"Space Weather Message Code": [], "Original Message": []}
    noaa_xray = {"Space Weather Message Code": [], "Original Message": []}

    # Set up potential time frame
    range_val = []
    if 'start_date' in parser_data and 'end_date' in parser_data:
        start_time_dt = dt.strptime(parser_data['start_date'], "%Y-%m-%d")
        end_time_dt = dt.strptime(parser_data['end_date'], "%Y-%m-%d")
        range_val = [start_time_dt, end_time_dt]

    for file in html_input_list:
        noaa_file = open(file, "r")
        lines = noaa_file.readlines()

        new_event = False
        current_event = {}
        message = ''

        for line in lines:

            # Set up new event and check for formatting
            if new_event:
                current_event = {}
                message = ''
                new_event = False
                if "Space Weather Message Code" not in line:
                    print("Something went wrong, SWMC not first parameter of new event")
                    break

            # <hr> indicates a new event
            if "<hr>" in line:
                if len(current_event) > 0:
                    current_event['Original Message'] = message
                    noaa_events.append(current_event)  # save old event to output list
                new_event = True

            # Go through parameters, process and save to current event
            message += line
            if ":" in line:
                split_line = line.split(":")
                parameter = split_line[0].strip()
                if "<p>" in parameter:
                    parameter = parameter.split("<p>")[1].strip()
                value = split_line[1]
                if "<br>" in value:
                    value = value.split("<br>")[0].strip()
                current_event[parameter] = value

        # Initialize dictionaries with correct variable options
        for alert in noaa_events:
            if "Space Weather Message Code" not in alert:
                continue

            swmc = alert["Space Weather Message Code"]

            for key in alert.keys():

                if "ALTEF" in swmc:
                    if key not in noaa_electronflux:
                        noaa_electronflux[key] = []
                elif "K0" in swmc or "SUD" in swmc or "WATA" in swmc:
                    if key not in noaa_geomagnetic:
                        noaa_geomagnetic[key] = []
                elif "ALTPC" in swmc or "ALTPX" in swmc or "SUMPC" in swmc or "SUMPX" in swmc \
                        or "WARPC" in swmc or "WARPX" in swmc:
                    if key not in noaa_proton:
                        noaa_proton[key] = []
                elif "ALTTP" in swmc or "10R" in swmc:
                    if key not in noaa_radio:
                        noaa_radio[key] = []
                elif "STR" in swmc:
                    if key not in noaa_stratospheric:
                        noaa_stratospheric[key] = []
                elif "XMF" in swmc or "SUMX" in swmc:
                    if key not in noaa_xray:
                        noaa_xray[key] = []
                else:
                    print("Event not captured in one of the expected categories, SWMC: ", swmc)

    # Empty dicts with the correct parameter lists for each category
    # print("Electron-flux dict", noaa_electronflux)
    # print("Geomagnetic dict", noaa_geomagnetic)
    # print("Proton dict", noaa_proton)
    # print("Radio dict", noaa_radio)
    # print("Stratospheric dict", noaa_stratospheric)
    # print("Xray dict", noaa_xray)

    # Add alerts to correct dictionaries
    for alert in noaa_events:

        if "Space Weather Message Code" not in alert:
            # print("Unexpected alert, skipping", alert)
            continue

        # If there is a time range setting
        if len(range_val) > 1:
            issue_time_val = alert["Issue Time"].strip()
            alert_date = dt.strptime(issue_time_val, "%Y %b %d %H%M %Z")
            if range_val[1] > alert_date > range_val[0]:
                pass
            else:
                continue

        swmc = alert["Space Weather Message Code"]

        if "ALTEF" in swmc:
            for possible_variable in noaa_electronflux.keys():
                if possible_variable == 'Original Message':
                    noaa_electronflux['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_electronflux[possible_variable].append(alert[possible_variable])
                else:
                    noaa_electronflux[possible_variable].append(np.nan)

        elif "K0" in swmc or "SUD" in swmc or "WATA" in swmc:
            for possible_variable in noaa_geomagnetic.keys():
                if possible_variable == 'Original Message':
                    noaa_geomagnetic['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_geomagnetic[possible_variable].append(alert[possible_variable])
                else:
                    noaa_geomagnetic[possible_variable].append(np.nan)

        elif "ALTPC" in swmc or "ALTPX" in swmc or "SUMPC" in swmc or "SUMPX" in swmc \
                or "WARPC" in swmc or "WARPX" in swmc:
            for possible_variable in noaa_proton.keys():
                if possible_variable == 'Original Message':
                    noaa_proton['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_proton[possible_variable].append(alert[possible_variable])
                else:
                    noaa_proton[possible_variable].append(np.nan)

        elif "ALTTP" in swmc or "10R" in swmc:
            for possible_variable in noaa_radio.keys():
                if possible_variable == 'Original Message':
                    noaa_radio['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_radio[possible_variable].append(alert[possible_variable])
                else:
                    noaa_radio[possible_variable].append(np.nan)

        elif "STR" in swmc:
            for possible_variable in noaa_stratospheric.keys():
                if possible_variable == 'Original Message':
                    noaa_stratospheric['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_stratospheric[possible_variable].append(alert[possible_variable])
                else:
                    noaa_stratospheric[possible_variable].append(np.nan)

        elif "XMF" in swmc or "SUMX" in swmc:
            for possible_variable in noaa_xray.keys():
                if possible_variable == 'Original Message':
                    noaa_xray['Original Message'].append(alert['Original Message'])
                elif possible_variable in alert.keys():
                    noaa_xray[possible_variable].append(alert[possible_variable])
                else:
                    noaa_xray[possible_variable].append(np.nan)

    # Filled dictionaries with html-read events
    # print("Electron-flux dict", noaa_electronflux)
    # print("Geomagnetic dict", noaa_geomagnetic)
    # print("Proton dict", noaa_proton)
    # print("Radio dict", noaa_radio)
    # print("Stratospheric dict", noaa_stratospheric)
    # print("Xray dict", noaa_xray)

    # Convert dictionaries to pd.DataFrames and csv
    electronflux_df = pd.DataFrame(noaa_electronflux)
    geomagnetic_df = pd.DataFrame(noaa_geomagnetic)
    proton_df = pd.DataFrame(noaa_proton)
    radio_df = pd.DataFrame(noaa_radio)
    stratospheric_df = pd.DataFrame(noaa_stratospheric)
    xray_df = pd.DataFrame(noaa_xray)

    return_dict = {'electronflux': electronflux_df, 'geomagnetic': geomagnetic_df, 'proton': proton_df,
                   'radio': radio_df, 'stratospheric': stratospheric_df, 'xray': xray_df}

    # return electronflux_df, geomagnetic_df, proton_df, radio_df, stratospheric_df, xray_df
    return return_dict


def parse_noaa_recent_json_to_dataframe(notifications: dict = -1, parser_data: dict = -1) -> dict:
    """Read recent NOAA data from dictionary and process to initial dataframes

    Parameters
    ----------
    notifications : dict
        A dictionary with all the notifications in the recent json file.
    parser_data : dict
        A dictionary with the settings from the argument parser.

    Returns
    -------
    A dictionary with dataframes with events for the following event types
        * Electron-flux events
        * Geomagnetic events
        * Proton events
        * Radio events
        * Stratospheric events
        * X-ray events
    """

    noaa_electronflux = {"Space Weather Message Code": [], "Original Message": []}
    noaa_geomagnetic = {"Space Weather Message Code": [], "Original Message": []}
    noaa_proton = {"Space Weather Message Code": [], "Original Message": []}
    noaa_radio = {"Space Weather Message Code": [], "Original Message": []}
    noaa_stratospheric = {"Space Weather Message Code": [], "Original Message": []}
    noaa_xray = {"Space Weather Message Code": [], "Original Message": []}

    # Set up potential time frame
    range_val = []
    print('parser data', parser_data)
    if 'start_date' in parser_data and 'end_date' in parser_data:
        start_time_dt = dt.strptime(parser_data['start_date'], "%Y-%m-%d")
        end_time_dt = dt.strptime(parser_data['end_date'], "%Y-%m-%d")
        range_val = [start_time_dt, end_time_dt]

    # Initialize dictionaries with correct variable options
    for alert in notifications:

        # Get SWMC
        message = alert['message']
        split_message = message.split("\r\n")
        split_code = split_message[0].split(":")[1]

        # Group notifications by SWMC and get their unique parameters
        for variable in split_message:
            if ":" not in variable:
                split_variable = variable.split("-")
            else:
                split_variable = variable.split(":")

            if "ALTEF" in split_code:
                if split_variable[0] not in noaa_electronflux:
                    noaa_electronflux[split_variable[0]] = []
            elif "K0" in split_code or "SUD" in split_code or "WATA" in split_code:
                if split_variable[0] not in noaa_geomagnetic:
                    noaa_geomagnetic[split_variable[0]] = []
            elif "ALTPC" in split_code or "ALTPX" in split_code or "SUMPC" in split_code or "SUMPX" in split_code or "WARPC" in split_code or "WARPX" in split_code:
                if split_variable[0] not in noaa_proton:
                    noaa_proton[split_variable[0]] = []
            elif "ALTTP" in split_code or "10R" in split_code:
                if split_variable[0] not in noaa_radio:
                    noaa_radio[split_variable[0]] = []
            elif "STR" in split_code:
                if split_variable[0] not in noaa_stratospheric:
                    noaa_stratospheric[split_variable[0]] = []
            elif "XMF" in split_code or "SUMX" in split_code:
                if split_variable[0] not in noaa_xray:
                    noaa_xray[split_variable[0]] = []
            else:
                print("Event not captured in one of the expected categories", split_code)

    # Empty dicts with the correct parameter lists for each category
    # print("Electron-flux dict", noaa_electronflux)
    # print("Geomagnetic dict", noaa_geomagnetic)
    # print("Proton dict", noaa_proton)
    # print("Radio dict", noaa_radio)
    # print("Stratospheric dict", noaa_stratospheric)
    # print("Xray dict", noaa_xray)

    # Add alerts to correct dictionaries
    for alert in notifications:

        message = alert['message']
        split_message = message.split("\r\n")
        split_code = split_message[0].split(":")[1]

        variables_tmp = {}

        # If there is a time range setting
        if len(range_val) > 1:
            issue_time_val = alert["issue_datetime"].strip()
            alert_date = dt.strptime(issue_time_val, "%Y-%m-%d %H:%M:%S.%f")
            if range_val[1] > alert_date > range_val[0]:
                pass
            else:
                continue

        for variable in split_message:
            if ":" not in variable:
                split_variable = variable.split("-")
            else:
                split_variable = variable.split(":")
            if len(split_variable) > 1:
                variables_tmp[split_variable[0]] = split_variable[1]

        if "ALTEF" in split_code:
            for possible_variable in noaa_electronflux.keys():
                if possible_variable == 'Original Message':
                    noaa_electronflux['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_electronflux[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_electronflux[possible_variable].append(np.nan)

        elif "K0" in split_code or "SUD" in split_code or "WATA" in split_code:
            for possible_variable in noaa_geomagnetic.keys():
                if possible_variable == 'Original Message':
                    noaa_geomagnetic['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_geomagnetic[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_geomagnetic[possible_variable].append(np.nan)

        elif "ALTPC" in split_code or "ALTPX" in split_code or "SUMPC" in split_code or "SUMPX" in split_code or "WARPC" in split_code or "WARPX" in split_code:
            for possible_variable in noaa_proton.keys():
                if possible_variable == 'Original Message':
                    noaa_proton['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_proton[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_proton[possible_variable].append(np.nan)

        elif "ALTTP" in split_code or "10R" in split_code:
            for possible_variable in noaa_radio.keys():
                if possible_variable == 'Original Message':
                    noaa_radio['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_radio[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_radio[possible_variable].append(np.nan)

        elif "STR" in split_code:
            for possible_variable in noaa_stratospheric.keys():
                if possible_variable == 'Original Message':
                    noaa_stratospheric['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_stratospheric[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_stratospheric[possible_variable].append(np.nan)

        elif "XMF" in split_code or "SUMX" in split_code:
            for possible_variable in noaa_xray.keys():
                if possible_variable == 'Original Message':
                    noaa_xray['Original Message'].append(message)
                elif possible_variable in variables_tmp:
                    noaa_xray[possible_variable].append(variables_tmp[possible_variable])
                else:
                    noaa_xray[possible_variable].append(np.nan)

    # Filled dictionaries with recent NOAA events
    # print("Electron-flux dict", noaa_electronflux)
    # print("Geomagnetic dict", noaa_geomagnetic)
    # print("Proton dict", noaa_proton)
    # print("Radio dict", noaa_radio)
    # print("Stratospheric dict", noaa_stratospheric)
    # print("Xray dict", noaa_xray)

    # Convert dictionaries to pd.DataFrames and csv
    electronflux_df = pd.DataFrame(noaa_electronflux)
    geomagnetic_df = pd.DataFrame(noaa_geomagnetic)
    proton_df = pd.DataFrame(noaa_proton)
    radio_df = pd.DataFrame(noaa_radio)
    stratospheric_df = pd.DataFrame(noaa_stratospheric)
    xray_df = pd.DataFrame(noaa_xray)

    return_dict = {'electronflux': electronflux_df, 'geomagnetic': geomagnetic_df, 'proton': proton_df,
                   'radio': radio_df, 'stratospheric': stratospheric_df, 'xray': xray_df}

    # return electronflux_df, geomagnetic_df, proton_df, radio_df, stratospheric_df, xray_df
    return return_dict


def convert_noaa_electronflux(dataframe_list: list = -1) -> list:
    """Convert NOAA electron-flux dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA electron-flux events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one electron-flux event.
    """

    electronflux_objects = \
        {
            'alerts': []
        }
    electronflux_notifications = []

    # Loop over all input dataframes
    for events_df in dataframe_list:

        # Make dataframes for all types of electron flux events
        alerts_electronflux_df = pd.DataFrame(columns=events_df.keys())

        for index, row in events_df.iterrows():
            if "ALTEF" in row["Space Weather Message Code"]:
                alerts_electronflux_df.loc[len(alerts_electronflux_df)] = row

        # Drop empty columns
        alerts_electronflux_df = alerts_electronflux_df.dropna(axis=1, how='all')


        # Generate electron flux event objects
        for index, row in alerts_electronflux_df.iterrows():
            # Continued electron flux message
            if "CONTINUED ALERT" in row and len(str(row["CONTINUED ALERT"])) > 3:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "continued_alert": row["CONTINUED ALERT"],
                        "continuation_serial_number": row["Continuation of Serial Number"],
                        "begin_time": row["Begin Time"],
                        "yesterday_max_flux": row["Yesterday Maximum 2MeV Flux"],
                        "event_type": "alert",
                        "continued": True,
                        "original_message": row["Original Message"],
                    }

            # Initial electron flux message
            elif "ALERT" in row:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "alert": row["ALERT"],
                        "threshold_reached": row["Threshold Reached"],
                        "station": row["Station"],
                        "event_type": "alert",
                        "continued": False,
                        "original_message": row["Original Message"],
                    }

            tmp_object = ElectronFluxEvent(settings)
            electronflux_objects['alerts'].append(tmp_object)

    # Electron flux alerts
    for electronflux_alert in electronflux_objects['alerts']:
        electronflux_alert.generate_message()
        electronflux_timeline_viewer_entry = {'product_id': electronflux_alert.swmc.strip(),
                                              'issue_datetime': electronflux_alert.issue_time.strip(),
                                              'end_time': '',
                                              'message': electronflux_alert.potential_impacts
                                                         + electronflux_alert.additional_information,
                                              'original_message': electronflux_alert.original_message}
        if hasattr(electronflux_alert, "begin_time"):
            electronflux_timeline_viewer_entry['begin_time'] = electronflux_alert.begin_time.strip()
        else:
            electronflux_timeline_viewer_entry['begin_time'] = ''
        electronflux_notifications.append(electronflux_timeline_viewer_entry)

    return electronflux_notifications


def convert_noaa_geomagnetic(dataframe_list: list = -1) -> list:
    """Convert NOAA geomagnetic dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA geomagnetic events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one geomagnetic event.
    """

    geomagnetic_objects = \
        {
            'alerts': [],
            'summaries': [],
            'warnings': [],
            'watches': [],
        }
    geomagnetic_notifications = []

    # Loop over all input dataframes
    for events_df in dataframe_list:

        # Drop month-based columns to reduce unnecessary columns
        month_list = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec',
                      'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        for col_name in events_df.keys():
            for month in month_list:
                if month in col_name:
                    events_df = events_df.drop(columns=[col_name])

        # Make dataframes for all types of geomagnetic events
        alerts_geomagnetic_df = pd.DataFrame(columns=events_df.keys())
        summaries_geomagnetic_df = pd.DataFrame(columns=events_df.keys())
        warnings_geomagnetic_df = pd.DataFrame(columns=events_df.keys())
        watches_geomagnetic_df = pd.DataFrame(columns=events_df.keys())

        # Counter to manually adjust number of geomagnetic events processed
        counter = 0
        limiter = 1e6
        for index, row in events_df.iterrows():
            if counter > limiter:
                break
            counter += 1
            if "ALT" in row["Space Weather Message Code"]:
                alerts_geomagnetic_df.loc[len(alerts_geomagnetic_df)] = row
            elif "SUM" in row["Space Weather Message Code"]:
                summaries_geomagnetic_df.loc[len(summaries_geomagnetic_df)] = row
            elif "WAR" in row["Space Weather Message Code"]:
                warnings_geomagnetic_df.loc[len(warnings_geomagnetic_df)] = row
            elif "WATA" in row["Space Weather Message Code"]:
                watches_geomagnetic_df.loc[len(watches_geomagnetic_df)] = row

        # Drop empty columns
        alerts_geomagnetic_df = alerts_geomagnetic_df.dropna(axis=1, how='all')
        summaries_geomagnetic_df = summaries_geomagnetic_df.dropna(axis=1, how='all')
        warnings_geomagnetic_df = warnings_geomagnetic_df.dropna(axis=1, how='all')
        watches_geomagnetic_df = watches_geomagnetic_df.dropna(axis=1, how='all')

        # Strip whitespaces from headers
        alerts_geomagnetic_df = alerts_geomagnetic_df.rename(columns=lambda x: x.strip())
        summaries_geomagnetic_df = summaries_geomagnetic_df.rename(columns=lambda x: x.strip())
        warnings_geomagnetic_df = warnings_geomagnetic_df.rename(columns=lambda x: x.strip())
        watches_geomagnetic_df = watches_geomagnetic_df.rename(columns=lambda x: x.strip())

        # Generate geomagnetic event objects
        for index, row in alerts_geomagnetic_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "comment": row["Comment"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],
                        "alert": row["CANCEL ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "alert": row["ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],

                        "noaa_scale": row["NOAA Scale"],
                        "threshold_reached": row["Threshold Reached"],
                        "synoptic_period": row["Synoptic Period"],
                        "active_warning": row["Active Warning"],
                    }
                if hasattr(row, "Potential Impacts") and len(str(row["Potential Impacts"])) > 3:
                    settings["potential_impacts_base"] = row["Potential Impacts"]
                else:
                    settings["potential_impacts_base"] = ""
            tmp_object = GeomagneticEvent(settings)
            geomagnetic_objects['alerts'].append(tmp_object)

        for index, row in summaries_geomagnetic_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "comment": row["Comment"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],
                        "summary": row["CANCEL SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "summary": row["SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],

                        "observed": row["Observed"],
                        "deviation": row["Deviation"],
                        "station": row["Station"]
                    }
            tmp_object = GeomagneticEvent(settings)
            geomagnetic_objects['summaries'].append(tmp_object)

        for index, row in warnings_geomagnetic_df.iterrows():
            if "K0" in row["Space Weather Message Code"]:
                if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                        row["Cancel Serial Number"]) > 0:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "warning": row["CANCEL WARNING"],
                            "event_type": "warning",
                            "geomagnetic_event": "K-index",
                            "original_message": row["Original Message"],

                            "comment": row["Comment"],
                            "cancel_serial_number": row["Cancel Serial Number"]
                        }
                else:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "warning": row["WARNING"],
                            "event_type": "warning",
                            "geomagnetic_event": "K-index",
                            "original_message": row["Original Message"],

                            "valid_from": row["Valid From"],
                            "valid_to": row["Valid To"],
                            "noaa_scale": row["NOAA Scale"],
                        }
                    if len(str(row["Warning Condition"])) > 3 and "ersistence" in row["Warning Condition"]:
                        settings["extended_warning"] = row["EXTENDED WARNING"]
                        settings["extension_serial_number"] = row["Extension to Serial Number"]
                    if hasattr(row, "Potential Impacts") and len(str(row["Potential Impacts"])) > 3:
                        settings["potential_impacts_base"] = row["Potential Impacts"]
                    else:
                        settings["potential_impacts_base"] = ""

            elif "SUD" in row["Space Weather Message Code"]:
                if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                        row["Cancel Serial Number"]) > 0:
                    # Cancellation of previous warning
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "warning": row["CANCEL WARNING"],
                            "event_type": "warning",
                            "geomagnetic_event": "sudden_impulse",
                            "original_message": row["Original Message"],

                            "comment": row["Comment"],
                            "cancel_serial_number": row["Cancel Serial Number"]
                        }
                else:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "warning": row["WARNING"],
                            "event_type": "warning",
                            "geomagnetic_event": "sudden_impulse",
                            "original_message": row["Original Message"],

                            "valid_from": row["Valid From"],
                            "valid_to": row["Valid To"],
                            "ip_shock_passage": row["IP Shock Passage Observed"]
                        }

            tmp_object = GeomagneticEvent(settings)
            geomagnetic_objects['warnings'].append(tmp_object)

        for index, row in watches_geomagnetic_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                # Cancellation of previous watch
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "watch": row["CANCEL WATCH"],
                        "event_type": "watch",
                        "comment": row["Comment"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "original_message": row["Original Message"],

                        "potential_impacts_base": row["Potential Impacts"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "watch": row["WATCH"],
                        "event_type": "watch",
                        "original_message": row["Original Message"],

                        "potential_impacts_base": row["Potential Impacts"],
                    }
            tmp_object = GeomagneticEvent(settings)
            geomagnetic_objects['watches'].append(tmp_object)

    # Geomagnetic alerts
    for geomagnetic_alert in geomagnetic_objects['alerts']:
        geomagnetic_alert.generate_message()
        geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_alert.swmc.strip(),
                                             'issue_datetime': geomagnetic_alert.issue_time.strip(),
                                             'begin_time': '',
                                             'end_time': '',
                                             'noaa_scale': 'Not available',
                                             'message': 'Not available',
                                             'original_message': geomagnetic_alert.original_message}
        if hasattr(geomagnetic_alert, "noaa_scale") and len(str(geomagnetic_alert.noaa_scale)) > 3:
            geomagnetic_timeline_viewer_entry['noaa_scale'] = geomagnetic_alert.noaa_scale
        if hasattr(geomagnetic_alert, "description") and len(str(geomagnetic_alert.description)) > 3:
            geomagnetic_timeline_viewer_entry['message'] = geomagnetic_alert.description
        geomagnetic_notifications.append(geomagnetic_timeline_viewer_entry)

    # Geomagnetic summaries
    for geomagnetic_summary in geomagnetic_objects['summaries']:
        geomagnetic_summary.generate_message()
        geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_summary.swmc.strip(),
                                             'issue_datetime': geomagnetic_summary.issue_time.strip(),
                                             'begin_time': '',
                                             'end_time': '',
                                             'noaa_scale': 'Not available',
                                             'message': 'Not available',
                                             'original_message': geomagnetic_summary.original_message}
        if hasattr(geomagnetic_summary, "description") and len(str(geomagnetic_summary.description)) > 3:
            geomagnetic_timeline_viewer_entry['message'] = geomagnetic_summary.description
        if hasattr(geomagnetic_summary, "observed") and len(str(geomagnetic_summary.observed)) > 3:
            geomagnetic_timeline_viewer_entry['begin_time'] = geomagnetic_summary.observed.strip()
        geomagnetic_notifications.append(geomagnetic_timeline_viewer_entry)

    # Geomagnetic warnings
    for geomagnetic_warning in geomagnetic_objects['warnings']:
        geomagnetic_warning.generate_message()
        if "K0" in geomagnetic_warning.swmc:
            if hasattr(geomagnetic_warning, "cancel_serial_number"):
                geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_warning.swmc.strip(),
                                                     'issue_datetime': geomagnetic_warning.issue_time.strip(),
                                                     'begin_time': '',
                                                     'end_time': '',
                                                     'noaa_scale': 'Not available',
                                                     'message': 'Not available',
                                                     'original_message': geomagnetic_warning.original_message}
            else:
                geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_warning.swmc.strip(),
                                                     'issue_datetime': geomagnetic_warning.issue_time.strip(),
                                                     'begin_time': '',
                                                     'end_time': '',
                                                     'noaa_scale': 'Not available',
                                                     'message': 'Not available',
                                                     'original_message': geomagnetic_warning.original_message}
            if hasattr(geomagnetic_warning, "noaa_scale") and len(str(geomagnetic_warning.noaa_scale)) > 3:
                geomagnetic_timeline_viewer_entry['noaa_scale'] = geomagnetic_warning.noaa_scale
            if hasattr(geomagnetic_warning, "valid_from") and len(str(geomagnetic_warning.valid_from)) > 3:
                geomagnetic_timeline_viewer_entry['begin_time'] = geomagnetic_warning.valid_from.strip()
            if hasattr(geomagnetic_warning, "valid_to") and len(str(geomagnetic_warning.valid_to)) > 3:
                geomagnetic_timeline_viewer_entry['end_time'] = geomagnetic_warning.valid_to.strip()
            if hasattr(geomagnetic_warning, "description") and len(str(geomagnetic_warning.description)) > 3:
                geomagnetic_timeline_viewer_entry['message'] = geomagnetic_warning.description
        elif "SUD" in geomagnetic_warning.swmc and geomagnetic_warning.warning_condition == "regular":
            if hasattr(geomagnetic_warning, "cancel_serial_number"):
                geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_warning.swmc.strip(),
                                                     'issue_datetime': geomagnetic_warning.issue_time.strip(),
                                                     'begin_time': '',
                                                     'end_time': '',
                                                     'noaa_scale': 'Not available',
                                                     'message': 'Not available',
                                                     'original_message': geomagnetic_warning.original_message}
            else:
                geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_warning.swmc.strip(),
                                                     'issue_datetime': geomagnetic_warning.issue_time.strip(),
                                                     'begin_time': '',
                                                     'end_time': '',
                                                     'noaa_scale': 'Not available',
                                                     'message': 'Not available',
                                                     'original_message': geomagnetic_warning.original_message}
                if hasattr(geomagnetic_warning, "valid_from") and len(str(geomagnetic_warning.valid_from)) > 3:
                    geomagnetic_timeline_viewer_entry['begin_time'] = geomagnetic_warning.valid_from.strip()
                if hasattr(geomagnetic_warning, "valid_to") and len(str(geomagnetic_warning.valid_to)) > 3:
                    geomagnetic_timeline_viewer_entry['end_time'] = geomagnetic_warning.valid_to.strip()
                if hasattr(geomagnetic_warning, "description") and len(str(geomagnetic_warning.description)) > 3:
                    geomagnetic_timeline_viewer_entry['message'] = geomagnetic_warning.description
        else:
            geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_warning.swmc.strip(),
                                                 'issue_datetime': geomagnetic_warning.issue_time.strip(),
                                                 'begin_time': '',
                                                 'end_time': '',
                                                 'noaa_scale': 'Not available',
                                                 'message': 'Not available',
                                                 'original_message': geomagnetic_warning.original_message}
            if hasattr(geomagnetic_warning, "valid_from") and len(str(geomagnetic_warning.valid_from)) > 3:
                geomagnetic_timeline_viewer_entry['begin_time'] = geomagnetic_warning.valid_from.strip()
            if hasattr(geomagnetic_warning, "valid_to") and len(str(geomagnetic_warning.valid_to)) > 3:
                geomagnetic_timeline_viewer_entry['end_time'] = geomagnetic_warning.valid_to.strip()
            if hasattr(geomagnetic_warning, "description") and len(str(geomagnetic_warning.description)) > 3:
                geomagnetic_timeline_viewer_entry['message'] = geomagnetic_warning.description

        if '2023' in geomagnetic_timeline_viewer_entry['begin_time'] and \
                '2026' in geomagnetic_timeline_viewer_entry['end_time']:
            pass
        else:
            geomagnetic_notifications.append(geomagnetic_timeline_viewer_entry)

    # Geomagnetic watches
    for geomagnetic_watch in geomagnetic_objects['watches']:
        geomagnetic_watch.generate_message()
        geomagnetic_timeline_viewer_entry = {'product_id': geomagnetic_watch.swmc.strip(),
                                             'issue_datetime': geomagnetic_watch.issue_time.strip(),
                                             'begin_time': '',
                                             'end_time': '',
                                             'noaa_scale': 'Not available',
                                             'message': 'Not available',
                                             'original_message': geomagnetic_watch.original_message}
        if hasattr(geomagnetic_watch, "description") and len(str(geomagnetic_watch.description)) > 3:
            geomagnetic_timeline_viewer_entry['message'] = geomagnetic_watch.description
        geomagnetic_notifications.append(geomagnetic_timeline_viewer_entry)

    return geomagnetic_notifications


def convert_noaa_proton(dataframe_list: list = -1) -> list:
    """Convert NOAA proton dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA proton events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one proton event.
    """

    proton_objects = \
        {
            'alerts': [],
            'warnings': [],
            'summaries': []
        }
    proton_notifications = []

    # Loop over all input dataframes, generate alerts, summaries and warnings df
    for events_df in dataframe_list:

        # Make dataframes for all types of proton events
        alerts_proton_df = pd.DataFrame(columns=events_df.keys())
        summaries_proton_df = pd.DataFrame(columns=events_df.keys())
        warnings_proton_df = pd.DataFrame(columns=events_df.keys())

        for index, row in events_df.iterrows():
            if "ALT" in row["Space Weather Message Code"]:
                alerts_proton_df.loc[len(alerts_proton_df)] = row
            elif "SUM" in row["Space Weather Message Code"]:
                summaries_proton_df.loc[len(summaries_proton_df)] = row
            elif "WAR" in row["Space Weather Message Code"]:
                warnings_proton_df.loc[len(warnings_proton_df)] = row

        # Drop empty columns
        alerts_proton_df = alerts_proton_df.dropna(axis=1, how='all')
        summaries_proton_df = summaries_proton_df.dropna(axis=1, how='all')
        warnings_proton_df = warnings_proton_df.dropna(axis=1, how='all')

        # Generate proton event objects
        for index, row in alerts_proton_df.iterrows():
            if len(str(row["Begin Time"])) <= 4:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "alert": row["Comment"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            elif "ALTPC" in row["Space Weather Message Code"]:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "alert": row["ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            elif "ALTPX" in row["Space Weather Message Code"]:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "alert": row["ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],

                        "noaa_scale": row["NOAA Scale"],
                    }
            tmp_object = ProtonEvent(settings)
            proton_objects['alerts'].append(tmp_object)

        for index, row in summaries_proton_df.iterrows():
            if len(str(row["Begin Time"])) <= 4:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "summary": row["Comment"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            elif "SUMPC" in row["Space Weather Message Code"]:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "maximum_time": row["Maximum Time"],
                        "end_time": row["End Time"],
                        "maximum_100mev_flux": row["Maximum 100MeV Flux"],
                        "summary": row["SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            elif "SUMPX" in row["Space Weather Message Code"]:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "maximum_time": row["Maximum Time"],
                        "end_time": row["End Time"],
                        "noaa_scale": row["NOAA Scale"],
                        "maximum_10mev_flux": row["Maximum 10MeV Flux"],
                        "summary": row["SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            tmp_object = ProtonEvent(settings)
            proton_objects['summaries'].append(tmp_object)

        for index, row in warnings_proton_df.iterrows():
            if "WARPC" in row["Space Weather Message Code"]:
                # First check for nan in warning condition (cancel warning)
                if len(str(row["Warning Condition"])) < 4:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "cancel_serial_number": row["Cancel Serial Number"],
                            "warning_condition": "Cancel",
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
                elif 'Onset' in row["Warning Condition"]:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "valid_from": row["Valid From"],
                            "warning_condition": row["Warning Condition"],
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
                elif 'ersistence' in row["Warning Condition"]:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "valid_from": row["Valid From"],
                            "extension_serial_number": row["Extension to Serial Number"],
                            "warning_condition": row["Warning Condition"],
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
            elif "WARPX" in row["Space Weather Message Code"]:
                # First check for nan in warning condition (cancel warning)
                if len(str(row["Warning Condition"])) < 4:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "cancel_serial_number": row["Cancel Serial Number"],
                            "warning_condition": "Cancel",
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
                elif 'Onset' in row["Warning Condition"]:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "valid_from": row["Valid From"],
                            "warning_condition": row["Warning Condition"],
                            "noaa_scale_predicted": row["Predicted NOAA Scale"],
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
                elif 'ersistence' in row["Warning Condition"]:
                    settings = \
                        {
                            "swmc": row["Space Weather Message Code"],
                            "serial_number": row["Serial Number"],
                            "issue_time": row["Issue Time"],
                            "valid_from": row["Valid From"],
                            "warning_condition": row["Warning Condition"],
                            "noaa_scale_predicted": row["Predicted NOAA Scale"],
                            "extension_serial_number": row["Extension to Serial Number"],
                            "event_type": "warning",
                            "original_message": row["Original Message"],
                        }
            if "warning condition" in settings and \
                    "Cancel" in settings["warning_condition"]:
                settings["warning"] = row["Comment"]
            elif len(str(row["WARNING"])) > 3:
                settings["warning"] = row["WARNING"]
                settings["valid_to"] = row["Valid To"]
            else:
                settings["warning"] = row["EXTENDED WARNING"]
                settings["extension_serial_number"] = row["Extension to Serial Number"]
                settings["valid_to"] = row["Now Valid Until"]

            tmp_object = ProtonEvent(settings)
            proton_objects['warnings'].append(tmp_object)

    # Proton alerts
    for proton_alert in proton_objects['alerts']:
        proton_alert.generate_message()
        if len(str(proton_alert.begin_time)) <= 3:
            proton_timeline_viewer_entry = {'product_id': proton_alert.swmc.strip(),
                                            'issue_datetime': proton_alert.issue_time.strip(),
                                            'begin_time': '',
                                            'end_time': '',
                                            'noaa_scale': 'Not available',
                                            'message': proton_alert.description,
                                            'original_message': proton_alert.original_message}
        elif "ALTPX" in proton_alert.swmc:
            if len(str(proton_alert.noaa_scale)) <= 3:
                proton_timeline_viewer_entry = {'product_id': proton_alert.swmc.strip(),
                                                'issue_datetime': proton_alert.issue_time.strip(),
                                                'begin_time': proton_alert.begin_time.strip(),
                                                'end_time': '',
                                                'noaa_scale': 'Not available',
                                                'message': proton_alert.description,
                                                'original_message': proton_alert.original_message}
            else:
                proton_timeline_viewer_entry = {'product_id': proton_alert.swmc.strip(),
                                                'issue_datetime': proton_alert.issue_time.strip(),
                                                'begin_time': proton_alert.begin_time.strip(),
                                                'end_time': '',
                                                'noaa_scale': proton_alert.noaa_scale,
                                                'message': proton_alert.description,
                                                'original_message': proton_alert.original_message}
        else:
            proton_timeline_viewer_entry = {'product_id': proton_alert.swmc.strip(),
                                            'issue_datetime': proton_alert.issue_time.strip(),
                                            'begin_time': proton_alert.begin_time.strip(),
                                            'end_time': '',
                                            'noaa_scale': 'Not available',
                                            'message': proton_alert.description,
                                            'original_message': proton_alert.original_message}
        if len(str(proton_timeline_viewer_entry['begin_time'])) <= 3:
            proton_timeline_viewer_entry['begin_time'] = ''
        if len(str(proton_timeline_viewer_entry['end_time'])) <= 3:
            proton_timeline_viewer_entry['end_time'] = ''
        proton_notifications.append(proton_timeline_viewer_entry)

    # Proton summaries
    for proton_summary in proton_objects['summaries']:
        proton_summary.generate_message()
        if len(str(proton_summary.begin_time)) <= 3:
            proton_timeline_viewer_entry = {'product_id': proton_summary.swmc.strip(),
                                            'issue_datetime': proton_summary.issue_time.strip(),
                                            'begin_time': '',
                                            'end_time': '',
                                            'noaa_scale': 'Not available',
                                            'message': proton_summary.description,
                                            'original_message': proton_summary.original_message}
        elif "SUMPX" in proton_summary.swmc:
            if len(str(proton_summary.noaa_scale)) <= 3:
                proton_timeline_viewer_entry = {'product_id': proton_summary.swmc.strip(),
                                                'issue_datetime': proton_summary.issue_time.strip(),
                                                'begin_time': proton_summary.begin_time.strip(),
                                                'end_time': proton_summary.end_time.strip(),
                                                'noaa_scale': 'Not available',
                                                'message': proton_summary.description,
                                                'original_message': proton_summary.original_message}
            else:
                proton_timeline_viewer_entry = {'product_id': proton_summary.swmc.strip(),
                                                'issue_datetime': proton_summary.issue_time.strip(),
                                                'begin_time': proton_summary.begin_time.strip(),
                                                'end_time': proton_summary.end_time.strip(),
                                                'noaa_scale': proton_summary.noaa_scale,
                                                'message': proton_summary.description,
                                                'original_message': proton_summary.original_message}
        else:
            proton_timeline_viewer_entry = {'product_id': proton_summary.swmc.strip(),
                                            'issue_datetime': proton_summary.issue_time.strip(),
                                            'begin_time': proton_summary.begin_time.strip(),
                                            'end_time': proton_summary.end_time.strip(),
                                            'noaa_scale': 'Not available',
                                            'message': proton_summary.description,
                                            'original_message': proton_summary.original_message}
        if len(str(proton_timeline_viewer_entry['begin_time'])) <= 3:
            proton_timeline_viewer_entry['begin_time'] = ''
        if len(str(proton_timeline_viewer_entry['end_time'])) <= 3:
            proton_timeline_viewer_entry['end_time'] = ''
        proton_notifications.append(proton_timeline_viewer_entry)

    # Proton warnings
    for proton_warning in proton_objects['warnings']:
        proton_warning.generate_message()
        if "WARPX" in proton_warning.swmc:
            if 'Cancel' in proton_warning.warning_condition:
                proton_timeline_viewer_entry = {'product_id': proton_warning.swmc.strip(),
                                                'issue_datetime': proton_warning.issue_time.strip(),
                                                'begin_time': '',
                                                'end_time': '',
                                                'noaa_scale': 'Not available',
                                                'message': proton_warning.description,
                                                'original_message': proton_warning.original_message}
            else:
                if len(str(proton_warning.noaa_scale_predicted)) <= 3:
                    proton_timeline_viewer_entry = {'product_id': proton_warning.swmc.strip(),
                                                    'issue_datetime': proton_warning.issue_time.strip(),
                                                    'begin_time': proton_warning.valid_from.strip(),
                                                    'end_time': '',
                                                    'noaa_scale': 'Not available',
                                                    'message': proton_warning.description,
                                                    'original_message': proton_warning.original_message}
                else:
                    proton_timeline_viewer_entry = {'product_id': proton_warning.swmc.strip(),
                                                    'issue_datetime': proton_warning.issue_time.strip(),
                                                    'begin_time': proton_warning.valid_from.strip(),
                                                    'end_time': '',
                                                    'noaa_scale': proton_warning.noaa_scale_predicted,
                                                    'message': proton_warning.description,
                                                    'original_message': proton_warning.original_message}
        else:
            if 'Cancel' in proton_warning.warning_condition:
                proton_timeline_viewer_entry = {'product_id': proton_warning.swmc.strip(),
                                                'issue_datetime': proton_warning.issue_time.strip(),
                                                'begin_time': '',
                                                'end_time': '',
                                                'noaa_scale': 'Not available',
                                                'message': proton_warning.description,
                                                'original_message': proton_warning.original_message}
            else:
                proton_timeline_viewer_entry = {'product_id': proton_warning.swmc.strip(),
                                                'issue_datetime': proton_warning.issue_time.strip(),
                                                'begin_time': proton_warning.valid_from.strip(),
                                                'end_time': '',
                                                'noaa_scale': 'Not available',
                                                'message': proton_warning.description,
                                                'original_message': proton_warning.original_message}
        if len(str(proton_timeline_viewer_entry['begin_time'])) <= 3:
            proton_timeline_viewer_entry['begin_time'] = ''
        if len(str(proton_timeline_viewer_entry['end_time'])) <= 3:
            proton_timeline_viewer_entry['end_time'] = ''
        proton_notifications.append(proton_timeline_viewer_entry)

    return proton_notifications


def convert_noaa_radio(dataframe_list: list = -1) -> list:
    """Convert NOAA radio dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA radio events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one radio event.
    """

    radio_objects = \
        {
            'alerts': [],
            'summaries': []
        }
    radio_notifications = []

    # Loop over all dataframe inputs
    for events_df in dataframe_list:

        # Make dataframes for all types of radio events
        alerts_radio_df = pd.DataFrame(columns=events_df.keys())
        summaries_radio_df = pd.DataFrame(columns=events_df.keys())

        for index, row in events_df.iterrows():
            if "ALT" in row["Space Weather Message Code"]:
                alerts_radio_df.loc[len(alerts_radio_df)] = row
            elif "SUM" in row["Space Weather Message Code"]:
                summaries_radio_df.loc[len(summaries_radio_df)] = row

        # Drop empty columns
        alerts_radio_df = alerts_radio_df.dropna(axis=1, how='all')
        summaries_radio_df = summaries_radio_df.dropna(axis=1, how='all')

        # Generate radio event objects
        for index, row in alerts_radio_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],
                        "alert": row["CANCEL ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "estimated_velocity": row["Estimated Velocity"],
                        "alert": row["ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            tmp_object = RadioEvent(settings)
            radio_objects['alerts'].append(tmp_object)

        for index, row in summaries_radio_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],
                        "summary": row["CANCEL SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "begin_time": row["Begin Time"],
                        "summary": row["SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],

                        "maximum_time": row["Maximum Time"],
                        "end_time": row["End Time"],
                        "duration": row["Duration"],
                        "peak_flux": re.sub('\D', '', row["Peak Flux"]),
                        "latest_penticton_noon_flux": row["Latest Penticton Noon Flux"]
                    }
            tmp_object = RadioEvent(settings)
            radio_objects['summaries'].append(tmp_object)

    # Radio alerts
    for radio_alert in radio_objects['alerts']:
        radio_alert.generate_message()
        if 'Cancellation' in radio_alert.description:
            radio_timeline_viewer_entry = {'product_id': radio_alert.swmc.strip(),
                                           'issue_datetime': radio_alert.issue_time.strip(),
                                           'begin_time': '',
                                           'end_time': '',
                                           'peak_flux': np.nan,
                                           'message': radio_alert.description,
                                           'original_message': radio_alert.original_message}
        else:
            radio_timeline_viewer_entry = {'product_id': radio_alert.swmc.strip(),
                                           'issue_datetime': radio_alert.issue_time.strip(),
                                           'begin_time': radio_alert.begin_time.strip(),
                                           'end_time': '',
                                           'peak_flux': np.nan,
                                           'message': radio_alert.description,
                                           'original_message': radio_alert.original_message}
        radio_notifications.append(radio_timeline_viewer_entry)

    # Radio summaries
    for radio_summary in radio_objects['summaries']:
        radio_summary.generate_message()
        if 'Cancellation' in radio_summary.description:
            radio_timeline_viewer_entry = {'product_id': radio_summary.swmc.strip(),
                                           'issue_datetime': radio_summary.issue_time.strip(),
                                           'begin_time': '',
                                           'end_time': '',
                                           'peak_flux': np.nan,
                                           'message': radio_summary.description,
                                           'original_message': radio_summary.original_message}
        else:
            radio_timeline_viewer_entry = {'product_id': radio_summary.swmc.strip(),
                                           'issue_datetime': radio_summary.issue_time.strip(),
                                           'begin_time': radio_summary.begin_time.strip(),
                                           'end_time': radio_summary.end_time.strip(),
                                           'peak_flux': radio_summary.peak_flux,
                                           'message': radio_summary.description,
                                           'original_message': radio_summary.original_message}
        radio_notifications.append(radio_timeline_viewer_entry)

    return radio_notifications


def convert_noaa_stratospheric(dataframe_list: list = -1) -> list:
    """Convert NOAA stratospheric dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA stratospheric events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one stratospheric event.
    """

    stratospheric_objects = \
        {
            'alerts': []
        }
    stratospheric_notifications = []

    # Loop over all csv input files
    for events_df in dataframe_list:

        # Make dataframes for all types of stratospheric events
        alerts_stratospheric_df = pd.DataFrame(columns=events_df.keys())

        for index, row in events_df.iterrows():
            if "ALT" in row["Space Weather Message Code"]:
                alerts_stratospheric_df.loc[len(alerts_stratospheric_df)] = row

        # Drop empty columns
        alerts_stratospheric_df = alerts_stratospheric_df.dropna(axis=1, how='all')

        # Generate stratospheric event objects
        for index, row in alerts_stratospheric_df.iterrows():
            settings = \
                {
                    "swmc": row["Space Weather Message Code"],
                    "serial_number": row["Serial Number"],
                    "issue_time": row["Issue Time"],
                    "utc_day": row["Valid for UTC Day"],
                    "event_type": "alert",
                    "original_message": row["Original Message"],
                }
            tmp_object = StratosphericEvent(settings)
            stratospheric_objects['alerts'].append(tmp_object)

    # Stratospheric alerts
    for stratospheric_alert in stratospheric_objects['alerts']:
        stratospheric_timeline_viewer_entry = {'product_id': stratospheric_alert.swmc.strip(),
                                               'issue_datetime': stratospheric_alert.issue_time.strip(),
                                               'message': stratospheric_alert.comment,
                                               'original_message': stratospheric_alert.original_message}
        stratospheric_notifications.append(stratospheric_timeline_viewer_entry)

    return stratospheric_notifications


def convert_noaa_xray(dataframe_list: list = -1) -> list:
    """Convert x-ray proton dataframe input to a list of upload-ready events

    Parameters
    ----------
    dataframe_list : list
        A list with parsed dataframes for NOAA x-ray events.

    Returns
    -------
    A list with dictionaries. Each dictionary contains upload-ready
    information about one x-ray event.
    """

    xray_objects = \
        {
            'alerts': [],
            'summaries': []
        }
    xray_notifications = []

    # Loop over all csv input files
    for events_df in dataframe_list:

        # Make dataframes for all types of xray events
        alerts_xray_df = pd.DataFrame(columns=events_df.keys())
        summaries_xray_df = pd.DataFrame(columns=events_df.keys())

        for index, row in events_df.iterrows():
            if "ALT" in row["Space Weather Message Code"]:
                alerts_xray_df.loc[len(alerts_xray_df)] = row
            elif "SUM" in row["Space Weather Message Code"]:
                summaries_xray_df.loc[len(summaries_xray_df)] = row

        # Drop empty columns
        alerts_xray_df = alerts_xray_df.dropna(axis=1, how='all')
        summaries_xray_df = summaries_xray_df.dropna(axis=1, how='all')

        # Generate x-ray event objects
        for index, row in alerts_xray_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],
                        "alert": row["CANCEL ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],
                        "threshold_reached": row["Threshold Reached"],
                        "noaa_scale": row["NOAA Scale"],
                        "alert": row["ALERT"],
                        "event_type": "alert",
                        "original_message": row["Original Message"],
                    }
            tmp_object = XRayEvent(settings)
            xray_objects['alerts'].append(tmp_object)

        for index, row in summaries_xray_df.iterrows():
            if "Cancel Serial Number" in row and not isnan(float(row["Cancel Serial Number"])) and int(
                    row["Cancel Serial Number"]) > 0:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "cancel_serial_number": row["Cancel Serial Number"],
                        "issue_time": row["Issue Time"],

                        "summary": row["CANCEL SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }
            else:
                settings = \
                    {
                        "swmc": row["Space Weather Message Code"],
                        "serial_number": row["Serial Number"],
                        "issue_time": row["Issue Time"],

                        "begin_time": row["Begin Time"],
                        "end_time": row["End Time"],
                        "maximum_time": row["Maximum Time"],
                        "summary": row["SUMMARY"],
                        "event_type": "summary",
                        "original_message": row["Original Message"],
                    }

            # Optional summary entries
            if len(str(row["Location"])) > 3:
                settings["location"] = row["Location"]
            if len(str(row["X-ray Class"])) > 3:
                settings["xray_class"] = row["X-ray Class"]
            if len(str(row["NOAA Scale"])) > 3:
                settings["noaa_scale"] = row["NOAA Scale"]
            if len(str(row["Optical Class"])) > 3:
                settings["optical_class"] = row["Optical Class"]

            tmp_object = XRayEvent(settings)
            xray_objects['summaries'].append(tmp_object)

    # X-ray alerts
    for xray_alert in xray_objects['alerts']:
        xray_alert.generate_message()
        xray_timeline_viewer_entry = {'product_id': xray_alert.swmc.strip(),
                                      'issue_datetime': xray_alert.issue_time.strip(),
                                      'begin_time': '',
                                      'end_time': '',
                                      'message': xray_alert.potential_impacts + xray_alert.additional_information,
                                      'original_message': xray_alert.original_message}
        if hasattr(xray_alert, "noaa_scale") and len(str(xray_alert.noaa_scale)) > 3:
            xray_timeline_viewer_entry["noaa_scale"] = xray_alert.noaa_scale
        else:
            xray_timeline_viewer_entry["noaa_scale"] = 'Not available'
        xray_notifications.append(xray_timeline_viewer_entry)

    # X-ray summaries
    for xray_summary in xray_objects['summaries']:
        xray_summary.generate_message()
        xray_timeline_viewer_entry = {'product_id': xray_summary.swmc.strip(),
                                      'issue_datetime': xray_summary.issue_time.strip(),
                                      'message': xray_summary.potential_impacts + xray_summary.additional_information,
                                      'original_message': xray_summary.original_message}

        if hasattr(xray_summary, "noaa_scale") and len(str(xray_summary.noaa_scale)) > 3:
            xray_timeline_viewer_entry["noaa_scale"] = xray_summary.noaa_scale
        else:
            xray_timeline_viewer_entry["noaa_scale"] = 'Not available'

        if hasattr(xray_summary, "begin_time") and len(str(xray_summary.begin_time)) > 3:
            xray_timeline_viewer_entry["begin_time"] = xray_summary.begin_time.strip()
        else:
            xray_timeline_viewer_entry["begin_time"] = ''

        if hasattr(xray_summary, "end_time") and len(str(xray_summary.end_time)) > 3:
            xray_timeline_viewer_entry["end_time"] = xray_summary.end_time.strip()
        else:
            xray_timeline_viewer_entry["end_time"] = ''

        xray_notifications.append(xray_timeline_viewer_entry)

    return xray_notifications


# Temporary method to test directly from swxtools, commented for now
if __name__ == "__main__":

    # Download
    print("Start downloading")
    download_noaa_html()
    recent_notifications = download_noaa_recent_json()
    print("Finished downloading")

    # Make list of html files
    read_list = []
    path = config['local_source_data_path'] + '/noaa_events'

    for root, dirc, files in walk(path):
        for FileName in files:
            if '.zip' not in FileName:
                read_list.append(path + '/' + FileName)

    # Parse
    print("Start parsing")
    archive_dataframes = parse_noaa_html_to_dataframe(read_list)
    recent_dataframes = parse_noaa_recent_json_to_dataframe(recent_notifications)
    print("Finished parsing")

    print("Archive dataframes", archive_dataframes)
    print("Recent dataframes", recent_dataframes)

    # Convert
    print("Start converting")
    electronflux_notifications_final = convert_noaa_electronflux([archive_dataframes['electronflux']])
    geomagnetic_notifications_final = convert_noaa_geomagnetic([archive_dataframes['geomagnetic']])
    proton_notifications_final = convert_noaa_proton([archive_dataframes['proton']])
    radio_notifications_final = convert_noaa_radio([archive_dataframes['radio']])
    stratospheric_notifications_final = convert_noaa_stratospheric([archive_dataframes['stratospheric']])
    xray_notifications_final = convert_noaa_xray([archive_dataframes['xray']])
    print("Finished converting")

    # Print results
    print("Print results")
    print("Electron-flux events", electronflux_notifications_final)
    print("Geomagnetic events", geomagnetic_notifications_final)
    print("Proton events", proton_notifications_final)
    print("Radio events", radio_notifications_final)
    print("Stratospheric events", stratospheric_notifications_final)
    print("X-ray events", xray_notifications_final)
    print("Finished printing results")
