import logging
from collections import Counter
import re
import csv
from glob import glob
import warnings
import json
import pandas as pd

# TODO add documentation

def cleantxt(txt):
    return txt.split(':')[-1].strip()


def parse_location_code(code):
    """ Check if code contains one of these bands:
    HNH - High latitudes Northern Hemisphere (northern region poleward of
          geographic latitude 60° N)
    MNH - Middle latitudes Northern Hemisphere (geographic latitude band
          between 30° N and 60° N)
    EQN - Equatorial latitudes Northern Hemisphere (geographic latitude band
          between 0° and 30° N)
    EQS - Equatorial latitudes Southern Hemisphere (geographic latitude band
          between 0° and 30° S)
    MSH - Middle latitudes Southern Hemisphere (geographic latitude band
          between 30° S and 60° S)
    HSH - High latitudes Southern Hemisphere (southern region poleward of
          geographic latitude 60° S)
    followed by a longitude (LONG) range, specified in 15° increments.
    """
    lats = []
    if "HNH" in code:
        lats.append(60)
        lats.append(90)
    if "MNH" in code:
        lats.append(30)
        lats.append(60)
    if "EQN" in code:
        lats.append(0)
        lats.append(30)
    if "EQS" in code:
        lats.append(-30)
        lats.append(0)
    if "MSH" in code:
        lats.append(-60)
        lats.append(-30)
    if "HSH" in code:
        lats.append(-90)
        lats.append(-60)

    # If a lat appears twice, it means it's a consecutive block and we need
    # to remove both lats
    c = Counter(lats)
    elements = set(c.elements())
    for elem in elements:
        if c[elem] > 1:
            lats = [lat for lat in lats if lat != elem]

    # Now for the longitude use a regular expression
    m1 = re.findall('[E][0-9]{3}', code)
    m2 = re.findall('[W][0-9]{3}', code)
    lons = []
    for lon in m1:
        value = float(lon[1:4])
        lons.append(value)
    for lon in m2:
        value = -1. * float(lon[1:4])
        lons.append(value)

    try:
        min_lon = min(lons)
        max_lon = max(lons)
    except ValueError:
        logging.warning("Could not parse longitudes: 'OBS SWX' field in advisory probably contains illegal text")
        min_lon = 0
        max_lon = 0

    if not lats:
        return min_lon, max_lon, [0, 0]

    return min_lon, max_lon, lats


def generate_csv_file_from_advisories(fPath, fname):
    """
    Script to read and verify SW-advisories.
    Author: M.L. Kooreman
    """
    fList = sorted(glob(fPath + 'fnxx01*.txt'))

    header = ['Arrival number',
              'Header type',
              'Header center',
              'Header time',
              'STATUS',
              'DTG',
              'SWXC',
              'ADVISORY NR',
              'NR RPLC',
              'SWX EFFECT',
              'SWX severity',
              'OBS SWX',
              'FCST SWX +6 HR',
              'FCST SWX +12 HR',
              'FCST SWX +18 HR',
              'FCST SWX +24 HR',
              'RMK',
              'NXT ADVISORY']

    arr_nr = 0
    fnxx_list = ['FNXX01', 'FNXX02', 'FNXX03', 'FNXX04']
    station_list = ['YMMC', 'LFPW', 'ZBBB', 'UUAG', 'EFKL', 'EGRR', 'KWNP']

    # Set parameters
    status = 'VOID'
    nr_rplc = 'VOID'

    outf = open(fname, 'w', encoding='UTF8', newline='')
    writer = csv.writer(outf)
    writer.writerow(header)

    rmk = ''

    for file in fList:
        for line in open(file):

            if line.startswith('ZCZC'):
                # new advisory: reset parameters
                status = 'VOID'
                nr_rplc = 'VOID'
                arr_nr += 1

            elif line.startswith('FNXX'):
                fnxx, station, time = line.split()
                if fnxx not in fnxx_list:
                    warnings.warn('Unknown FNXX number...')
                if station not in station_list:
                    warnings.warn('Unknown station...')

            elif line.startswith('SWX ADVISORY'):
                pass

            elif line.startswith('STATUS:'):
                status = cleantxt(line)
                if status != 'TEST':
                    status = 'NULL'

            elif line.startswith('DTG:'):
                dtg = cleantxt(line)

            elif line.startswith('SWXC:'):
                center = cleantxt(line)

            elif line.startswith('ADVISORY NR:'):
                nr = cleantxt(line)

            elif line.startswith('NR RPLC:'):
                nr_rplc = cleantxt(line)

            elif line.startswith('SWX EFFECT:'):
                swa = cleantxt(line)
                sev = swa.split()[-1]
                swx = swa.rsplit(' ', 1)[0]

            elif line.startswith('OBS SWX:'):
                obs = cleantxt(line)

            elif line.startswith('FCST SWX  +6 HR:'):
                fc06 = cleantxt(line)
            elif line.startswith('FCST SWX +6 HR:'):
                fc06 = cleantxt(line)

            elif line.startswith('FCST SWX +12 HR:'):
                fc12 = cleantxt(line)

            elif line.startswith('FCST SWX +18 HR:'):
                fc18 = cleantxt(line)

            elif line.startswith('FCST SWX +24 HR:'):
                fc24 = cleantxt(line)

            elif line.startswith('RMK:'):
                rmk = cleantxt(line)
            elif line.startswith(' '):
                rmk += ' ' + cleantxt(line)

            elif line.startswith('NXT ADVISORY:'):
                nxt = cleantxt(line)
                if nxt[-1] == '=':  # remove closing = statement
                    nxt = nxt[:-1]

            elif line.startswith('NNNN'):
                writer.writerow(
                    [arr_nr, fnxx, station, time, status, dtg, center, nr, nr_rplc, swx, sev, obs, fc06, fc12, fc18,
                     fc24, rmk, nxt])

                # reset parameters
                status = 'VOID'
                nr_rplc = 'VOID'

            else:
                rmk += ' ' + cleantxt(line)

    outf.close()


def parse_partial_date(dtg, partial_dt):
    timestamp_dtg = pd.to_datetime(dtg)
    day_of_month_dtg = timestamp_dtg.day
    month_dtg = timestamp_dtg.month
    year_dtg = timestamp_dtg.year
    partial_day_of_month, partial_time = partial_dt.split('/')
    if int(partial_day_of_month) < 2 and day_of_month_dtg > 26:
        # we have skipped to a new month, and need to figure out which month we're in
        partial_year = year_dtg
        partial_month = month_dtg + 1
        if partial_month == 13:  # a December to January skip
            partial_month = 1  # so set to January
            partial_year = partial_year + 1  # and increase the year
    else:
        partial_year = year_dtg
        partial_month = month_dtg
    timestring = f"{partial_year}-{partial_month}-{partial_day_of_month}T{partial_time}"
    return pd.to_datetime(timestring)


def parse_advisory_and_combine_into_events(record, df):
    output = {}
    obs_swx_time = record['OBS SWX'].split()[0]
    output['center_country'] = record['Header center']
    output['advisory_number'] = record['ADVISORY NR']
    output['obs_swx_text'] = record['OBS SWX'][9:]
    for forecasttime in [6, 12, 18, 24]:
        value = record[f'FCST SWX +{forecasttime} HR']
        output[f'forecast_{forecasttime}h_text'] = value[9:]
        output[f'forecast_{forecasttime}h_date'] = parse_partial_date(record['DTG'], value[:9]).isoformat()
    output['time_issued'] = pd.to_datetime(record['DTG']).isoformat()
    output['time_observed'] = parse_partial_date(record['DTG'], obs_swx_time).isoformat()
    output['effect'] = record['SWX EFFECT']
    output['severity'] = record['SWX severity']
    output['center'] = record['SWXC']
    output['nr_replacement'] = record['NR RPLC']
    output['remark'] = record['RMK']
    output['next_advisory'] = record['NXT ADVISORY']
    # Check if replacements exist
    replacement_condition = ((df['NR RPLC'] == record['ADVISORY NR']) & (df['SWXC'] == record['SWXC']) & (
                df['SWX EFFECT'] == record['SWX EFFECT']))
    output['num_replacements'] = len(df[replacement_condition])
    replacements = df[replacement_condition]
    output['replacements'] = []
    for replacement in replacements.to_dict(orient='records'):
        output['replacements'].append(parse_advisory_and_combine_into_events(replacement, df))
    # output['replacement nrs'] = replacements['ADVISORY NR'].values
    return output


def parse_advisory(record):
    output = {}
    obs_swx_time = record['OBS SWX'].split()[0]
    output['center_country'] = record['Header center']
    output['advisory_number'] = record['ADVISORY NR']
    output['obs_swx_text'] = record['OBS SWX'][9:]
    for forecasttime in [6, 12, 18, 24]:
        value = record[f'FCST SWX +{forecasttime} HR']
        output[f'forecast_{forecasttime}h_text'] = value[9:]
        output[f'forecast_{forecasttime}h_date'] = parse_partial_date(record['DTG'], value[:9]).isoformat()
    output['time_issued'] = pd.to_datetime(record['DTG']).isoformat()
    output['time_observed'] = parse_partial_date(record['DTG'], obs_swx_time).isoformat()
    output['effect'] = record['SWX EFFECT']
    output['severity'] = record['SWX severity']
    output['center'] = record['SWXC']
    output['nr_replacement'] = record['NR RPLC']
    output['remark'] = record['RMK']
    output['next_advisory'] = record['NXT ADVISORY']
    return output


def generate_json_from_advisory_csv(csv_fname, json_fname):
    # Load csv file into pandas dataframe
    df_full = pd.read_csv(csv_fname, keep_default_na=False)

    # Ignore advisories if status field = TEST or TEST is mentioned in the remarks field
    df_notest = df_full[(df_full['STATUS'] != 'TEST') & (~df_full['RMK'].str.contains('TEST'))]

    # Make a subset of only 'starting advisories' (so without advisories that
    # replace another one)
    df_original = df_notest[df_notest['NR RPLC'] == 'VOID']

    # Loop over all 'starting advisories', parse them, and put them in a list.
    parsed_list = []
    for record in df_original.to_dict(orient='records'):
        try:
            # Subsequent advisories that replace previous ones are handled in the
            # parsing method and are included as nested dictionaries. In order to
            # find advisories replacing an original one, the whole set needs to be
            # passed to the parsing method as well (df_notest)
            # TODO where does parse_record come from?
            parsed = parse_record(record, df_notest)
            parsed_list.append(parsed)
        except ValueError:
            logging.error(f"Could not parse advisory with number {record['ADVISORY NR']}")
            print(record)

    # Write the list of parsed advisories into a json file
    with open(json_fname, 'w') as fh:
        fh.write(json.dumps(parsed_list))

    return parsed_list


def generate_graphical_elements_json(grouped_advisories, ge_json_filename):
    plotitems = {}
    colors = {'MOD': 'orange', 'SEV': 'red'}
    # line connecting first and last advisory in chain
    lines = []
    for advisory in grouped_advisories:
        if len(advisory['replacements']) > 0:
            lines.append({'type': 'line',
                          'x1': advisory['time_issued'],
                          'x2': advisory['replacements'][-1]['time_issued'],
                          'y1': 0,
                          'y2': 0,
                          'stroke-width': '3pt',
                          'stroke-color': colors[advisory["severity"]],
                          })

    # line indicating time of observation
    for advisory in grouped_advisories:
        if len(advisory['replacements']) > 0:
            lines.append({'type': 'line',
                          'x1': advisory['time_observed'],
                          'x2': advisory['time_observed'],
                          'y1': -1,
                          'y2': 1,
                          'stroke-width': '0.5pt',
                          'stroke-color': 'black',
                          })
        for replacement in advisory['replacements']:
            lines.append({'type': 'line',
                          'x1': replacement['time_observed'],
                          'x2': replacement['time_observed'],
                          'y1': -1,
                          'y2': 1,
                          'stroke-width': '0.5pt',
                          'stroke-color': 'black',
                          })

    # Line connecting observation with advisory symbols:
    for advisory in grouped_advisories:
        if len(advisory['replacements']) > 0:
            lines.append({'type': 'line',
                          'x1': advisory['time_observed'],
                          'x2': advisory['time_issued'],
                          'y1': 0,
                          'y2': 0,
                          'stroke-width': '0.5pt',
                          'stroke-color': 'black',
                          })
        for replacement in advisory['replacements']:
            lines.append({'type': 'line',
                          'x1': replacement['time_observed'],
                          'x2': replacement['time_issued'],
                          'y1': 0,
                          'y2': 0,
                          'stroke-width': '0.5pt',
                          'stroke-color': 'black',
                          })

    plotitems['lines'] = lines

    # symbol for main advisory
    arcs = []
    for advisory in grouped_advisories:
        arcs.append({'type': 'arc',
                     'x': advisory['time_issued'],
                     'innerRadius': 0,
                     'outerRadius': 5,
                     'startAngle': 3.141592,
                     'endAngle': 2 * 3.141592,
                     'stroke-width': 1,
                     'stroke-color': colors[advisory["severity"]],
                     'fill': colors[advisory["severity"]],
                     })

    # symbol for extra (extension) advisories
    for advisory in grouped_advisories:
        if len(advisory['replacements']) > 0:
            for replacement in advisory['replacements'][:-1]:
                arcs.append({'type': 'arc',
                             'x': replacement['time_issued'],
                             'innerRadius': 0,
                             'outerRadius': 5,
                             'startAngle': 3.141592,
                             'endAngle': 2 * 3.141592,
                             'stroke-width': 1,
                             'stroke-color': colors[replacement["severity"]],
                             'fill': 'none',
                             })

    # symbol for last adivsory in group
    for advisory in grouped_advisories:
        if len(advisory['replacements']) > 0:
            arcs.append({'type': 'arc',
                         'x': advisory['replacements'][-1]['time_issued'],
                         'innerRadius': 0,
                         'outerRadius': 5,
                         'startAngle': 0,
                         'endAngle': 3.141592,
                         'stroke-width': 1,
                         'stroke-color': colors[advisory["severity"]],
                         'fill': 'none',
                         })

    plotitems['arcs'] = arcs

    # text for type of advisory
    texts = []
    for advisory in grouped_advisories:
        texts.append({'type': 'text',
                      'text': f"{advisory['effect']}",
                      'x': advisory['time_observed'],
                      'y': 0,
                      'xshift': -8,
                      'yshift': 0,
                      'text-anchor': 'end',
                      'dominant-baseline': 'middle'
                      })

    # text for advisory number and
    for advisory in grouped_advisories:
        texts.append({'type': 'text',
                      'text': f"{advisory['advisory_number']}",
                      'x': advisory['time_issued'],
                      'y': 0,
                      'xshift': 0,
                      'yshift': -12,
                      'text-anchor': 'middle',
                      'dominant-baseline': 'middle'
                      })
        texts.append({'type': 'text',
                      'text': f"{advisory['center']}",
                      'x': advisory['time_issued'],
                      'y': 0,
                      'xshift': 0,
                      'yshift': 12,
                      'text-anchor': 'middle',
                      'dominant-baseline': 'middle'
                      })
        for replacement in advisory['replacements']:
            texts.append({'type': 'text',
                          'text': f"{replacement['advisory_number']}",
                          'x': replacement['time_issued'],
                          'y': 0,
                          'xshift': 0,
                          'yshift': -12,
                          'text-anchor': 'middle',
                          'dominant-baseline': 'middle'
                          })
            texts.append({'type': 'text',
                          'text': f"{replacement['center']}",
                          'x': replacement['time_issued'],
                          'y': 0,
                          'xshift': 0,
                          'yshift': 12,
                          'text-anchor': 'middle',
                          'dominant-baseline': 'middle'
                          })

    plotitems['texts'] = texts

    with open(ge_json_filename, 'w') as fh:
        fh.write(json.dumps(plotitems))
