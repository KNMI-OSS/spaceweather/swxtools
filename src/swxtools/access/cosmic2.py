import logging
import glob
import tarfile
import re
import pandas as pd
import xarray as xr
from swxtools import download_tools
from swxtools.config import config
from swxtools.orbit import sp3

valid_data_types = ['leoOrb', 'ivmL2m']

if source == 'ucar':
    local_dir = config['local_source_data_path'] + '/cosmic2_ucar/'
    base_url = 'https://data.cosmic.ucar.edu/gnss-ro/cosmic2/'
    subdirs = {
        'leoOrb': 'nrt/level1b/',
        'ivmL2m': 'postProc/level2/',
    }
    # ivmL2m_C2E1.2022.001.01_2022.0770_nc
    # leoOrb_     2024.001.01_0001.0001_sp3
    # leoOrb_2022.001.001.01_0001.0001_sp3
    regexprs = {
        'leoOrb': r'leoOrb_(?P<year>\d{4})\.(?P<doy>\d{3})\.(?P<satid>\d{3})\.(?P<dump>\d{2})_(?P<subtype>\d{4})\.(?P<version>\d{4})_sp3',
        'ivmL2m': r'ivmL2m_C2E(?P<satid>\d).(?P<year>\d{4}).(?P<doy>\d{3}).(?P<dump>\d{2})_(?P<subtype>\d{4}).(?P<version>\d{4})_nc'
    }

elif source == 'tacc':
    # https://tacc.cwa.gov.tw/data-service/fs7rt_tdpc/level2/ivmL2m/
    local_dir = config['local_source_data_path'] + '/cosmic2_tacc/'
    base_url = 'https://tacc.cwa.gov.tw/data-service/'
    subdirs = {
        'leoOrb': 'fs7rt_tdpc/level1b/leoOrb/',
        'ivmL2m': 'fs7rt_tdpc/level2/ivmL2m/',
    }
    regexprs = {
        # 'leoOrb': r'leoOrb_(?P<year>\d{4})\.(?P<doy>\d{3})\.(?P<satid>\d{3})\.(?P<dump>\d{2})_(?P<subtype>\d{4})\.(?P<version>\d{4})_sp3',
        'ivmL2m': r'(?P<year>\d{4})\.(?P<doy>\d{3})/ivmL2m_C2E(?P<satid>\d).(?P<year>\d{4}).(?P<doy>\d{3}).(?P<dump>\d{2})_(?P<subtype>\d{4}).(?P<version>\d{4})_nc'
    }


def download(data_type, t0, t1):
    all_files = []
    for date in pd.date_range(t0, t1, freq='1D'):
        year = date.year
        doy = date.day_of_year
        logging.info(f"Downloading COSMIC-2 {data_type} data for {date.strftime('%Y-%m-%d')}")
        files = download_tools.mirror(
            base_url=base_url,
            sub_url=f'{subdirs[data_type]}/{year:04d}/{doy:03d}/',
            include_strings=[data_type],
            local_dir=local_dir)
        all_files.append(files)


def to_dataframe(sat, data_type, t0, t1):
    dfs = []
    for date in pd.date_range(t0, t1, freq='1D'):
        year = date.year
        doy = date.day_of_year
        globpattern = f"{local_dir}/{subdirs[data_type]}/{year:04d}/{doy:03d}/*.tar.gz"
        tarfiles = sorted(glob.glob(globpattern))
        if len(tarfiles) != 1:
            logging.error(f"Incorrect number of tarfiles in directory: {globpattern}. {len(tarfiles)} found, 1 expected.")
            continue
        tf = tarfile.open(tarfiles[0], mode='r:gz')
        for mem in tf.getmembers():
            filename = mem.name
            m = re.match(regexprs[data_type], filename)
            satid = m.groupdict()['satid']
            if int(satid) == int(sat):
                logging.info(f"Reading {filename}")
                # print(f"Reading {filename}")
                with tf.extractfile(mem) as fh:
                    if data_type == 'leoOrb':
                        dfs.append(sp3.sp3_to_itrf_df(fh))
                    else:
                        dfs.append(xr.load_dataset(fh).to_dataframe())
    # Concatenate the files
    if len(dfs) > 0:
        df = pd.concat(dfs, axis=0)
        # Keep only the latest records in case of duplicates
        df = df[~df.index.duplicated(keep='last')].sort_index()
        return df
    else:
        return pd.DataFrame()