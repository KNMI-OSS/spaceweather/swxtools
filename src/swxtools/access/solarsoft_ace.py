import pandas as pd
import requests
from swxtools import download_tools
from swxtools.config import config
import os
import logging
import numpy as np


valid_types = ['ace_swepam_1m', 'ace_mag_1m', 'ace_epam_5m']


def download(t0, t1, data_type):
    t_start = pd.to_datetime("20010811")
    t_end = pd.Timestamp.now().ceil('D')
    dates = pd.date_range(t0, t1, freq='1D')
    for t in dates:
        if t.tz_localize(None) < t_start:
            continue
        if t.tz_localize(None) > t_end:
            continue
        filename = f"{t.strftime('%Y%m%d')}_ace_swepam_1m.txt"
        local_source_data_path = config['local_source_data_path'] + f'/solarsoft/{data_type}'
        download_tools.ensure_data_dir(local_source_data_path)
        local_path = local_source_data_path + '/' + filename
        age = t_end.tz_localize(None) - t.tz_localize(None)
        if (not os.path.isfile(local_path)) or age < pd.to_timedelta(5, 'D'):
            url = f'https://sohoftp.nascom.nasa.gov/sdb/goes/ace/daily/{filename}'
            logging.info(f"Downloading {url}")
            r = requests.get(url)
            if r.ok:
                with open(local_path, 'w') as fh:
                    fh.write(r.text)
            else:
                logging.error(f"Error opening {url}: {r.status_code} {r.reason}")
                continue


def filename_to_dataframe(filename):
    if 'ace_swepam_1m' in filename:
        columns = [
            'year',
            'month',
            'day',
            'hhmm',
            'mjd',
            'seconds',
            'status',
            'proton_density',
            'bulk_speed',
            'ion_temperature'
        ]
    elif 'ace_epam_5m' in filename:
        [
            'year',
            'month',
            'day',
            'hhmm',
            'mjd',
            'seconds',
            'electron_status',
            'electron_38_53',
            'electron_175_315',
            'proton_status',
            'proton_47_68',
            'proton_115_195',
            'proton_310_580',
            'proton_795_1193',
            'proton_1060_1900',
            'anis_index'
        ]
    elif 'ace_mag_1m' in filename:
        [
          "status",
           "Bx",
           "By",
           "Bz",
           "Bt",
           "latitude",
           "longitude"
        ]
    else:
        logging.error(f"Unknown data ACE data type for file: {filename}")


    def parse_dates(row):
        timestr = f"{int(row.year):04d}-{int(row.month):02d}-{int(row.day):02d}T{int(row.hhmm):04d}"
        return pd.to_datetime(timestr, utc=True)

    df = pd.read_table(filename, sep=r'\s+', skiprows=18, names=columns)
    df.index = df.apply(parse_dates, axis=1)
    df = df.drop(['year', 'month', 'day', 'hhmm', 'mjd', 'seconds'], axis=1)
    df = df.replace({-9999.9: np.nan, -1.00e+05: np.nan})
    return df


def to_dataframe(t0, t1, data_type='ace_swepam_1m'):
    local_source_data_path = (config['local_source_data_path'] +
                              f'/solarsoft/{data_type}')
    download(t0, t1, 'ace_swepam_1m')
    dates = pd.date_range(t0, t1, freq='1D')
    dfs = []
    for t in dates:
        filename = f"{t.strftime('%Y%m%d')}_ace_swepam_1m.txt"
        local_path = local_source_data_path + '/' + filename
        if os.path.isfile(local_path):
            dfs.append(filename_to_dataframe(local_path))
    return pd.concat(dfs, axis=0).sort_index()
