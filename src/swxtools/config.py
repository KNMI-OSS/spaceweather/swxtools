import os

import yaml

ISO_TIMEFMT = "%Y-%m-%dT%H:%M:%S.%fZ"


class Config:
    """Config allows reading configuration strings by key

    will first look in environment variables and then try
    a swxtools config file.

    Example for the key 'some_config':

    1. try the environment variable 'some_config'
    2. try the environment variable 'SOME_CONFIG'
    3. try either ./swxtools.cfg or ~/.swxtools.cfg for 'some_config'


    `.cfg` configuration should be YAML compatible key/value pairs
    """

    file_config = {}

    def __init__(self):

        config_path = "swxtools.cfg"
        if not os.path.isfile(config_path):
            config_path = os.path.expanduser("~/.swxtools.cfg")

        if os.path.isfile(config_path):
            self.file_config = yaml.safe_load(open(config_path))
        else:
            print("config file not found in ./swxtools.cfg and ~/.swxtools.cfg")

    def __getitem__(self, key):
        if key in os.environ:
            return os.environ[key]
        elif key.upper() in os.environ:
            return os.environ[key.upper()]
        elif key in self.file_config:
            return self.file_config[key]
        else:
            raise KeyError(key)

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default


config = Config()
