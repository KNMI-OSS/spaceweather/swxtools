import pandas as pd
import astropy.time

gps_epoch_tai = astropy.time.Time("1980-01-06T00:00:00", scale='tai')
gps_epoch_utc = astropy.time.Time("1980-01-06T00:00:00", scale='utc')

def sp3_to_itrf_df(filename_or_filehandle):
	def gps_to_utc(date_time_index):
		return pd.to_datetime(
				(gps_epoch_utc + (astropy.time.Time(date_time_index, scale='tai') -
			 	gps_epoch_tai)).iso, utc=True
			)

	def parse_sp3_epoch(epochline):
		(_, year, month, day, hour, minute, seconds) = epochline.split()
		return {'time_gps': pd.to_datetime(f"{year}-{month}-{day}T" +
									   	f"{hour}:{minute}:{seconds}")}
	
	def parse_sp3_position(positionline):
		x = positionline[4:18]
		y = positionline[18:32]
		z = positionline[32:46]
		return {'x_itrf': float(x),
				'y_itrf': float(y),
				'z_itrf': float(z)}
	
	def parse_sp3_velocity(velocityline):
		try:
			vx = velocityline[4:18]
			vy = velocityline[18:32]
			vz = velocityline[32:46]
			return {'vx_itrf': float(vx)/1e4,
					'vy_itrf': float(vy)/1e4,
					'vz_itrf': float(vz)/1e4}
		except ValueError:
			logging.error("Error parsing SP3 velocity line: " + velocityline)
	
	class FormatError(Exception):
		pass
	
	data = []
	if type(filename_or_filehandle) is str:
		with open(filename_or_filehandle, 'r') as fh:
			lines = fh.readlines()
	else:
		lines = filename_or_filehandle.readlines()
		if type(lines[0]) is bytes:
			lines =  [line.decode() for line in lines]
	
	# Sanity checking
	num_epochs = int(lines[0][32:39])
	num_satellites = int(lines[2][4:6])
	if num_satellites != 1:
		raise FormatError("Number of satellites in SP3 header is " +
					  	f"{num_satellites}. This code can handle only 1.")
	expected_lines = 23 + num_epochs * num_satellites * 3
	if (len(lines) != expected_lines):
		raise FormatError(f"Number of lines in file is {len(lines)}, while " +
					  	f"{expected_lines} was expected based on the " +
					  	"number of epochs and satellites in the header")
	
	for i_epoch in range(0, num_epochs):
		epoch_line_number = 22 + i_epoch * num_satellites * 3
		epoch = parse_sp3_epoch(lines[epoch_line_number])
		for i_satellite in range(num_satellites):
			position_line_number = (22 + i_epoch * num_satellites * 3 +
									i_satellite * 2 + 1)
			velocity_line_number = (22 + i_epoch * num_satellites * 3 +
									i_satellite * 2 + 2)
			position = parse_sp3_position(lines[position_line_number])
			velocity = parse_sp3_velocity(lines[velocity_line_number])
		data.append({**epoch, **position, **velocity})
	df = pd.DataFrame(data)
	df.index = gps_to_utc(df['time_gps'])
	df.index.name = 'time_utc'
	return df
