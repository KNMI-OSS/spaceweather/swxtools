# KNMI Space Weather Tools (swxtools)
This python package contains tools used in the space weather group of KNMI. It contains Python code to access the KNMI HAPI server, retrieve space weather data from various online sources, and create or manipulate satellite orbit ephemeris.

The package is extensively used from KNMI's HAPI ingest scripts:
https://gitlab.com/KNMI-OSS/spaceweather/hapi-ingest-scripts

## Acknowledgements
The code has been maintained and expanded with support from ESA through the Swarm Data Innovation and Science Cluster (Swarm DISC). For more information on Swarm DISC, please visit https://earth.esa.int/eogateway/activities/swarm-disc
